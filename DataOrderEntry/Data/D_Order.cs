﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Data
{
    public class D_Order:Connection
    {
        public int Pockets(decimal Length)
        {
            int QtyPockets = 0;

            using (SqlCommand cmd = new SqlCommand("buildserial_getpockets", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Length", Length));

                SqlParameter ResultParameter = new SqlParameter("@Quantity", 0);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    QtyPockets = Convert.ToInt16(cmd.Parameters["@OrderNo"].Value.ToString());

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }


            return QtyPockets;
        }

        public string GettingANSI(int DocumentID, string PO)
        {
            string ANSI = "";
            using (SqlCommand cmd = new SqlCommand("EDI_GettingANSI", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DocumentID", DocumentID));
                cmd.Parameters.Add(new SqlParameter("@PO", PO));

                SqlParameter ResultParameter = new SqlParameter("@ASNI", SqlDbType.VarChar, 1000);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    ANSI = cmd.Parameters["@ASNI"].Value.ToString();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }


            return ANSI;
        }
         
        public string NewOrderNO(int CustomerID, string PO, string POdate)
        {
            string OrderID = "";
            using (SqlCommand cmd = new SqlCommand("orderentry_neworderid", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CustomerID", CustomerID));
                cmd.Parameters.Add(new SqlParameter("@POnumber", PO));
                cmd.Parameters.Add(new SqlParameter("@PODate", POdate));

                SqlParameter ResultParameter = new SqlParameter("@OrderNo", SqlDbType.VarChar, 50);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    OrderID = cmd.Parameters["@OrderNo"].Value.ToString();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return OrderID;
        }

        public void CreateANSI(int ediID, string PO, string Ansi)
        {
            using (SqlCommand cmd = new SqlCommand("EDI_CerateANSI", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@edID", ediID));
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                cmd.Parameters.Add(new SqlParameter("@ANSI", Ansi));
                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
        }

        public void EDIcreateOrder(int edtsID, int edsID, int edID, string Value, int Element, string PO, int Line)
        {
            using (SqlCommand cmd = new SqlCommand("EDI_CreateOrder", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@edtsID", edtsID));
                cmd.Parameters.Add(new SqlParameter("@edsID", edsID));
                cmd.Parameters.Add(new SqlParameter("@edID", edID));
                cmd.Parameters.Add(new SqlParameter("@Value", Value));
                cmd.Parameters.Add(new SqlParameter("@Element", Element));
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                cmd.Parameters.Add(new SqlParameter("@Line", Line));
                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
        }

        public void EDI_UpdateBWSData(string Invoice, string Tracking, string Date, string HomeDepotPO)
        {
            using (SqlCommand cmd = new SqlCommand("EDI_UpdateBWSData", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Invoice", Invoice));
                cmd.Parameters.Add(new SqlParameter("@Tracking", Tracking));
                cmd.Parameters.Add(new SqlParameter("@Date", Date));
                cmd.Parameters.Add(new SqlParameter("@PO", HomeDepotPO));


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();


                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
        }

        public void BuildSeria(string orderid, int Serial, int OptionID, string Value)
        {
            using (SqlCommand cmd = new SqlCommand("buildSerialDetail", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OrderID", orderid));
                cmd.Parameters.Add(new SqlParameter("@SerialID", Serial));
                cmd.Parameters.Add(new SqlParameter("@OptionID", OptionID));
                cmd.Parameters.Add(new SqlParameter("@Value", Value));
           

                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();


                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
        }

        public int NewSerialID(string OrderID, int ProductID, int Line, int ShipType, string SaleDate, string DueDate, decimal Cost)
        {
            int SerialID = 0;
            using (SqlCommand cmd = new SqlCommand("Orderentry_NewSerial", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Orderid", OrderID));
                cmd.Parameters.Add(new SqlParameter("@ProductID", ProductID));
                cmd.Parameters.Add(new SqlParameter("@LineNumber", Line));
                cmd.Parameters.Add(new SqlParameter("@ShipType", ShipType));
                cmd.Parameters.Add(new SqlParameter("@SaleDate", SaleDate));
                cmd.Parameters.Add(new SqlParameter("@DueDate", DueDate));
                cmd.Parameters.Add(new SqlParameter("@Cost", Cost));

                SqlParameter ResultParameter = new SqlParameter("@SerialID", 0);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    SerialID = Convert.ToInt16(cmd.Parameters["@SerialID"].Value.ToString());

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return SerialID;
        }

        public int NewCustomerID()
        {
            int CustomerID = 0;
            using (SqlCommand cmd = new SqlCommand("enterorder_NewCustomerID", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter ResultParameter = new SqlParameter("@CustomerID", 0);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    CustomerID = Convert.ToInt16(cmd.Parameters["@CustomerID"].Value.ToString());

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return CustomerID;
        }

        public void NewCustomerData(int ID, string name, string company, string Address, int zip, string City, string State, string Country, string Phone,
                                    string Email, string Sidemark)
        {
            using (SqlCommand cmd = new SqlCommand("enterorder_newCustomerData", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CusID", ID));
                cmd.Parameters.Add(new SqlParameter("@CusName", name));
                cmd.Parameters.Add(new SqlParameter("@Company", company));
                cmd.Parameters.Add(new SqlParameter("@Address", Address));
                cmd.Parameters.Add(new SqlParameter("@Zip", zip));
                cmd.Parameters.Add(new SqlParameter("@City", City));
                cmd.Parameters.Add(new SqlParameter("@State", State));
                cmd.Parameters.Add(new SqlParameter("@Country", Country));
                cmd.Parameters.Add(new SqlParameter("@Phone", Phone));
                cmd.Parameters.Add(new SqlParameter("@Mail", Email));
                cmd.Parameters.Add(new SqlParameter("@Sidemark", Sidemark));




                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    throw new Exception("Error on Connection  ", e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

        }

        public DataTable CustomerList()
        {
            DataTable tdbCustomer = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("enterorder_GetCustomerList", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdbCustomer);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdbCustomer;
        }

        public DataTable ProductsList()
        {
            DataTable tdbProducts = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("enterorder_Products", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdbProducts);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdbProducts;
        }

        public DataTable ProductType(int ProductID)
        {
            DataTable tdbProductType = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("enterorder_ProductType", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@lpngID", ProductID));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdbProductType);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdbProductType;
        }

        public DataTable Carrier()
        {
            DataTable tdbCarrier = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("Orderentry_Carrier", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdbCarrier);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdbCarrier;
        }

        public DataTable OrderPrority()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("Orderentry_OrderPriority", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable MaterialList(int Category)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("Orderentry_getmaterial", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CatMaterial", Category));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable ProductFractions()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("Orderentry_ProductFractions", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable Oprationsystem(int ProductID, int OptionID)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("orderentry_getoperationsystem", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ProductID", ProductID));
                cmd.Parameters.Add(new SqlParameter("@optionID", OptionID));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable Headrail(int ProductID, int OptionID, int OperationType)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("orderentry_Headrail", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ProductID", ProductID));
                cmd.Parameters.Add(new SqlParameter("@optionID", OptionID));
                cmd.Parameters.Add(new SqlParameter("@OperationType", OperationType));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable BuildSerialOptions(int ProductID, int Serial)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("buildserial_options", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@ProductID", ProductID));
                cmd.Parameters.Add(new SqlParameter("@Serial", Serial));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public string BuildSerial_GetoptionValue(int OptionID)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("buildserial_getoptionvalue", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OptionID", OptionID));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            if(tdb.Rows.Count > 0)
            {
                return tdb.Rows[0][0].ToString();
            }
            else
            {
                return "";
            }
        }

        public decimal GetFractionValue(string Display)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("GetFractionvalue", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DisplayValue", Display));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            if (tdb.Rows.Count > 0)
            {
                return Convert.ToDecimal(tdb.Rows[0][0].ToString());
            }
            else
            {
                return 0;
            }
        }

        public DataTable TicketsCreated()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("printer_Getticketscreated", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
             
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable SerialDetails(int Serial)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("printer_getserialdetails", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@Serial", Serial));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable TicketPrintHeader(int Serial)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("PrintTicket_Header", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SerialID", Serial));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable Ordersnot810()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_OrdersNot810", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
   
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable EDIElements(int DocumentID)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_Elements", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DocumentsID", DocumentID));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public string EDIQuery(string Query, string PO, int Flag_Paramater)
        {
            DataSet ds = new DataSet();
            SqlDataAdapter da;
            string QueryDB = "";

            if (Flag_Paramater == 1)
            {
                 QueryDB = Query + "'" + PO + "'";
            }
            else if(Flag_Paramater == 0)
            {
                 QueryDB = Query;
            }

            try
            {
                da = new SqlDataAdapter(QueryDB, dConnection);
                da.Fill(ds, "tdb");
                if(ds.Tables["tdb"].Rows.Count > 0)
                {
                    return ds.Tables["tdb"].Rows[0][0].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }

        }

        public DataTable EDIPrint(int DocumentID)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_Print", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@DocumentID", DocumentID));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable EDI850Segments(string SegID)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_Get850Seg", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@SegID", SegID));

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }
    }
}
