﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class D_EDI:Connection
    {
        public void SaveSerialDetails(int OptionID, string Value, int Line)
        {
            using (SqlCommand cmd = new SqlCommand("EDI_SaveSerialDetails", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OptionID", OptionID));
                cmd.Parameters.Add(new SqlParameter("@Line", Line));
                cmd.Parameters.Add(new SqlParameter("@Value", Value));

                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
        }

        public int getOptionID(string OptionName)
        {
            int OptionID = 0;

            using (SqlCommand cmd = new SqlCommand("EDI_getOptionID", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@OptionName", OptionName));

                SqlParameter ResultParameter = new SqlParameter("@OptionID", 0);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    OptionID = Convert.ToInt16(cmd.Parameters["@OptionID"].Value.ToString());

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }


            return OptionID;
        }

        public int OrderExists(string PO)
        {
            int OptionID = 0;

            using (SqlCommand cmd = new SqlCommand("EDI_CheckOrderExist", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));

                SqlParameter ResultParameter = new SqlParameter("@Result", 0);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    OptionID = Convert.ToInt16(cmd.Parameters["@Result"].Value.ToString());

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }


            return OptionID;
        }

        public DataTable get856toCreate()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_856toCreate", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable get855toCreate()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_855toCreate", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable get810toCreate()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_810toCreate", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable get855DataDetail(string PO)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_get855DataDetails", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable OrderstoPrint()
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_OrderstoPrint", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable getLines(string PO)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_getLinesByOrder", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable OrderReport(string PO)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_GetOrderReport", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@POnumber", PO));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable getOrderHeader(string PO)
        {
            DataTable tdbProductType = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_getOrderHeader", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdbProductType);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdbProductType;
        }

        public DataTable getSerialHeader(string PO, int Line)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_SerialHeader", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                cmd.Parameters.Add(new SqlParameter("@Line", Line));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public DataTable getSerialDetails(string PO, int Line)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_getSerialDetail", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                cmd.Parameters.Add(new SqlParameter("@Line", Line));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public string NewOrderNO(int CustomerID, string PO, string POdate, string POcustomer, string POaddress, string POCity, string POstate, string POpostal)
        {
            string OrderID = "";
            using (SqlCommand cmd = new SqlCommand("orderentry_neworderid", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@CustomerID", CustomerID));
                cmd.Parameters.Add(new SqlParameter("@POnumber", PO));
                cmd.Parameters.Add(new SqlParameter("@PODate", POdate));
                cmd.Parameters.Add(new SqlParameter("@POCustomer", POcustomer));
                cmd.Parameters.Add(new SqlParameter("@POaddress", POaddress));
                cmd.Parameters.Add(new SqlParameter("@POcity", POCity));
                cmd.Parameters.Add(new SqlParameter("@POstate", POstate));
                cmd.Parameters.Add(new SqlParameter("@POpostal", POpostal));



                SqlParameter ResultParameter = new SqlParameter("@OrderNo", SqlDbType.VarChar, 50);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);


                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    OrderID = cmd.Parameters["@OrderNo"].Value.ToString();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return OrderID;
        }

        public DataTable getLineswithDetail(string PO)
        {
            DataTable tdb = new DataTable();
            //tdbTicket = null;
            using (SqlCommand cmd = new SqlCommand("EDI_getLines", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                try
                {
                    OpenConnetion();
                    SqlDataAdapter dat = new SqlDataAdapter(cmd);
                    dat.Fill(tdb);
                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }

            return tdb;
        }

        public void SaveShipment(string PO, string Date, string Tracking)
        {
            using (SqlCommand cmd = new SqlCommand("EDI_SaveShiment", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));
                cmd.Parameters.Add(new SqlParameter("@ShipOn", Date));
                cmd.Parameters.Add(new SqlParameter("@Tracking", Tracking));

                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
        }



        public string CreateL3invoice(string PO)
        {
            string L3invoice = "";

            using (SqlCommand cmd = new SqlCommand("EDI_CreateL3invoice", dConnection))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@PO", PO));

                SqlParameter ResultParameter = new SqlParameter("@l3Invoice", SqlDbType.VarChar, 50);
                ResultParameter.Direction = ParameterDirection.Output;
                cmd.Parameters.Add(ResultParameter);

    
                try
                {
                    OpenConnetion();
                    cmd.ExecuteNonQuery();
                    L3invoice = cmd.Parameters["@l3Invoice"].Value.ToString();

                }
                catch (Exception e)
                {
                    throw new Exception("Error to get Location Info  " + e.Message, e);
                }
                finally
                {
                    CloseConnection();
                    cmd.Dispose();
                }

            }
            return L3invoice;
        }

    }
}
