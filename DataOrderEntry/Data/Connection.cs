﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class Connection
    {
       // public string sCnn = @"data source = localhost\SQLEXPRESS01; initial catalog = Production; Trusted_Connection=True;";
        public string sCnn = @"data source = localhost\SQLEXPRESS01; initial catalog = ProductionBWS; Trusted_Connection=True;";
        public string sCnn2 = @"data source = BWSSV05; initial catalog = Production; User ID = sa; Password = BWSsql5%";
        public SqlConnection dConnection;

        public Connection()
        {
            dConnection = new SqlConnection(sCnn);
        }

        public void OpenConnetion()
        {
            try
            {
                if (dConnection.State == ConnectionState.Broken || dConnection.State == ConnectionState.Closed)
                {
                    dConnection.Open();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error Connection", e);
            }

        }

        public void CloseConnection()
        {
            try
            {
                if (dConnection.State == ConnectionState.Open)
                {
                    dConnection.Close();
                }
            }
            catch (Exception e)
            {
                throw new Exception("Error Connection", e);
            }
        }


    }
}
