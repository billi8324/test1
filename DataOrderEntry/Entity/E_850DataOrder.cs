﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class E_850DataOrder
    {

        public int edstID { get; set; }
        public int edsID { get; set; }
        public int edID { get; set; }
        public string Value { get; set; }
        public string POnumber { get; set; }
        public int Element { get; set; }
        public int Line { get; set; }
    }
}
