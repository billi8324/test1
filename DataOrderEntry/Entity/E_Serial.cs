﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
   public class E_Serial
    {
        public int Line { get; set; }
        public int ProductID { get; set; }
        public string Product { get; set; } 
       public decimal Width { get; set; }
        public decimal Lenght { get; set; }
        public string Fabric { get; set; }
        public string Lining { get; set; }
        public string Mount { get; set; }
        public string Operation { get; set; }
        public string Control { get; set; }
        public string CordPosition { get; set; }
        public string HeadRail { get; set; }
        public string Valance { get; set; }
        public string Banding { get; set; }
        public string Room { get; set; }
        public decimal Cost { get; set; }
        
    }
}
