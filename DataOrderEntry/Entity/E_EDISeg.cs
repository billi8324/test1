﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_EDISeg
    {
        public int edtsID { get; set; }
        public int edsID { get; set; }
        public int edID { get; set; }
        public string SegID { get; set; }
        public string SegName { get; set; }
        public int Element { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int StaticFlag { get; set; }
        public string Query { get; set; }
    }
}
