﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_Ordernot810
    {
        public string ProdOrder { get; set; }
        public string PONumber { get; set; }
        public string PO_Date { get; set; }
        public string CustomerName { get; set; }
    }
}
