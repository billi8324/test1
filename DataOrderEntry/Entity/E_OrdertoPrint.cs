﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_OrdertoPrint
    {
        public string BWSOrder { get; set; }
        public string POnumber { get; set; }
        public string POdate { get; set; }
        public string Customer { get; set; }
        public int Line { get; set; }
        public string Product { get; set; }
        public string OperationSystem { get; set; }
        public decimal Cost { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public int PostalCode { get; set; }
    }
}
