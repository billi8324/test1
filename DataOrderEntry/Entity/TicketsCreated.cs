﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class TicketsCreated
    {
        public int Serial { get; set; }
        public string OrderID { get; set; }
        public string SaleDate { get; set; }
        public string DueDate { get; set; }
        public string Sidemark { get; set; }
        public string Product { get; set; }
        public string OperatingSystem { get; set; }
    }
}
