﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_EDIOrder
    {
        public string BWSOrder { get; set; }
        public string POnumber { get; set; }
        public DateTime PODate { get; set; }
        public string Customer { get; set; }
        public int Line { get; set; }
        public string Product { get; set; }
        public string OperationSystem { get; set; }
        public decimal Cost { get; set; }
    }
}
