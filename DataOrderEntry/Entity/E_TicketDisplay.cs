﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_TicketDisplay
    {
        public int Serial { get; set; }
        public int OptionID { get; set; }
        public string OptionName { get; set; }
        public string Value { get; set; }
    }
}
