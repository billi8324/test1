﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_EDIOrders
    {
        public string BWSOrder { get; set; }
        public string POnumber { get; set; }
        public string PODate { get; set; }
        public string Customer { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string AfiliateID { get; set; }
    }
}
