﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_EDIPrint
    {
        public string PO { get; set; }
        public string BWS_Invoice { get; set; }
        public string PO_Date { get; set; }
        public string CustomerName { get; set; }
    }
}
