﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity
{
    public class E_EDIProdctionDetails
    {
        public string Value { get; set; }
        public int Line { get; set; }
        public int OptionId { get; set; }
    }
}
