﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Business;

namespace DataOrderEntry
{
    /// <summary>
    /// Interaction logic for Template_Flat.xaml
    /// </summary>
    public partial class Template_Flat : UserControl
    {
        public string Test
        {
            get { return "this is a tes"; }
        }

        B_Order OBJbUS = new B_Order();
        int ProductID = 0;
        public Template_Flat(int ProdID)
        {
            InitializeComponent();
            ProductID = ProdID;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            cmbFabric.ItemsSource = OBJbUS.MaterialList(1).DefaultView;
            cmbLenght.ItemsSource = OBJbUS.ProductFractions().DefaultView;
            cmbWidth.ItemsSource = OBJbUS.ProductFractions().DefaultView;
            cmbMount.Items.Add("Inside");
            cmbMount.Items.Add("Outside");
            cmbValance.Items.Add("Yes");
            cmbValance.Items.Add("No");
            cmbLinig.Items.Add("PRIVACY WHITE");
            cmbLinig.Items.Add("PRIVACY IVORY");
            cmbLinig.Items.Add("BLOCKOUT WHITE");
            cmbLinig.Items.Add("BLOCKOUT IVORY");
            cmbLinig.Items.Add("N/A");
            cmbOperation.ItemsSource = OBJbUS.Oprationsystem(ProductID, 114).DefaultView; //114 => Operation System Option
            cmbControl.Items.Add("LEFT");
            cmbControl.Items.Add("RIGHT");
            cmbCord.Items.Add("BACK");
            cmbCord.Items.Add("FORWARD");
            cmbBanding.Items.Add("YES");
            cmbBanding.Items.Add("NO");
            cmbGad.Items.Add("1/4");
            cmbGad.Items.Add("3/8");
            cmbGad.Items.Add("1/2");
            cmbGad.Items.Add("5/8");
            cmbGad.Visibility = System.Windows.Visibility.Hidden;
            
        }

        private void cmbOperation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //MessageBox.Show(cmbOperation.SelectedValue.ToString());
           cmbheadrail.ItemsSource = OBJbUS.Headrail(ProductID, 119, Convert.ToInt16(cmbOperation.SelectedValue.ToString())).DefaultView; //119 => HEAD RAIL TYPE
        }

        private void cmbheadrail_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OBJbUS.BuildSerial_GetoptionValue(Convert.ToInt32(cmbheadrail.SelectedValue.ToString()))== "2 IN 1")
            {
                cmbGad.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                cmbGad.Visibility = System.Windows.Visibility.Hidden;
            }
        }
    }
}
