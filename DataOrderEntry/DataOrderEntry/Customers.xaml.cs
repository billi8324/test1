﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Business;
using Entity;
using System.Data;



namespace DataOrderEntry
{
    /// <summary>
    /// Interaction logic for Customers.xaml
    /// </summary>
    public partial class Customers : Window
    {
        B_Order objBus = new B_Order();
        List<E_Customercs> Items = new List<E_Customercs>();
        E_Customercs Customer = new E_Customercs();
        public Customers()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBus.CustomerList();

            foreach(DataRow rw in tdb.Rows)
            {
                E_Customercs it = new E_Customercs
                {
                    CustomerID = Convert.ToInt32(rw[0].ToString()),
                    CustomerName = rw[1].ToString(),
                    Company = rw[2].ToString(),
                    Address = rw[3].ToString(),
                    Zip = Convert.ToInt32(rw[4].ToString()),
                    City = rw[5].ToString(),
                    State = rw[6].ToString(),
                    Country = rw[7].ToString(),
                    Phone = rw[8].ToString(),
                    Email = rw[9].ToString(),
                    Sidemark = rw[10].ToString()
                };
                Items.Add(it);
            }

            dgCustomers.ItemsSource = Items;

        }

        private void dgCustomers_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(dgCustomers != null)
            {
                if(dgCustomers.SelectedItems.Count > 0)
                {
                    DialogResult = true;
                }
            }
        }
    }
}
