﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataOrderEntry
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnOrder_Click(object sender, RoutedEventArgs e)
        {
            CustomerData call = new CustomerData();
            grid1.Children.Clear();
            grid1.Children.Add(call);
        }

        private void btnTickets_Click(object sender, RoutedEventArgs e)
        {
            TicketsDisplay call = new TicketsDisplay();
            grid1.Children.Clear();
            grid1.Children.Add(call);
        }

        private void btnEDI_Click(object sender, RoutedEventArgs e)
        {
            EDI call = new EDI();
            grid1.Children.Clear();
            grid1.Children.Add(call);
        }
    }
}
