﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Business;
using Entity;
using System.Data;
using Formulas;

namespace DataOrderEntry
{
    /// <summary>
    /// Interaction logic for CustomerData.xaml
    /// </summary>
    public partial class CustomerData : UserControl
    {
        B_Order objBusOrder = new B_Order();

        E_Customercs CustomerItem = new E_Customercs();
        Template_Flat call;
        List<E_Serial> Items = new List<E_Serial>();
        List<E_ProductionDetails> Items_Details = new List<E_ProductionDetails>();
        int ProductID = 0;

        public CustomerData()
        {
            InitializeComponent();
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {

            objBusOrder.NewCustomerData(Convert.ToInt32(txtCustomerID.Text), txtName.Text, txtCompany.Text, txtAdress1.Text, Convert.ToInt32(txtZip.Text),
                                         txtCity.Text, txtState.Text, txtCountry.Text, txtPhone.Text, txtEmail.Text, txtSidemark.Text);

            cmbPriority.ItemsSource = objBusOrder.OrderPrority().DefaultView;
            cmbShipvia.ItemsSource = objBusOrder.Carrier().DefaultView;

            var tab = tabOrder.Items[1] as TabItem;
            tab.IsEnabled = true;
            tab = tabOrder.Items[0] as TabItem;
            tab.IsEnabled = false;
            tabOrder.SelectedIndex++;
        }

        private void txtShipSame_Click(object sender, RoutedEventArgs e)
        {
            txtContact.Text = txtName.Text;
            txtShipAdd1.Text = txtAdress1.Text;
            txtShipCity.Text = txtCity.Text;
            txtShipState.Text = txtState.Text;
            txtShipZip.Text = txtZip.Text;
            txtShipCountry.Text = txtCountry.Text;
            txtShipPhone.Text = txtPhone.Text;
            txtShipEmail.Text = txtEmail.Text;


        }

        private void txtShipNext_Click(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBusOrder.ProductsList();
            cmbProductGroup.ItemsSource = tdb.DefaultView;
            DataTable tdbType = objBusOrder.ProductType(100);
            cmbProduc.ItemsSource = tdbType.DefaultView;

            var tab = tabOrder.Items[2] as TabItem;
            tab.IsEnabled = true;
            tab = tabOrder.Items[1] as TabItem;
            tab.IsEnabled = false;

            //cmbProduc.Items.Add("Flat Fold");
            //cmbProduc.Items.Add("Classic");
            //cmbProduc.Items.Add("Hobbled");
            //cmbProductGroup.Items.Add("Roman Shade");
            tabOrder.SelectedIndex++;
        }

        private void TabItem_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        private void cmbProduc_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbProduc.ItemsSource != null)
            {
                ProductID = Convert.ToInt32(cmbProduc.SelectedValue.ToString());
                    call = new Template_Flat(ProductID);
                    gridTemplate.Children.Clear();
                    gridTemplate.Children.Add(call);

            }
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
            txtCustomerID.Text = objBusOrder.NewCustomerID().ToString();
        }

        private void btnFind_Click(object sender, RoutedEventArgs e)
        {
            Customers frm = new Customers();
            frm.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            frm.ShowDialog();
            if (frm.DialogResult.HasValue && frm.DialogResult.Value)
             {
                 CustomerItem = (E_Customercs)frm.dgCustomers.SelectedItems[0];
             }

            txtCustomerID.Text = CustomerItem.CustomerID.ToString();
            txtName.Text = CustomerItem.CustomerName;
            txtSidemark.Text = CustomerItem.Sidemark;
            txtAdress1.Text = CustomerItem.Address;
            txtCity.Text = CustomerItem.City;
            txtState.Text = CustomerItem.State;
            txtZip.Text = CustomerItem.Zip.ToString();
            txtPhone.Text = CustomerItem.Phone;
            txtEmail.Text = CustomerItem.Email;
            txtCountry.Text = CustomerItem.Country;

        }

        private void ClearForm()
        {
            txtCustomerID.Text = string.Empty;
            txtName.Text = string.Empty;
            txtSidemark.Text = string.Empty;
            txtAdress1.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            txtZip.Text = string.Empty;
            txtPhone.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtCountry.Text = string.Empty;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            var tab = tabOrder.Items[1] as TabItem;
            tab.IsEnabled = false;
            tab = tabOrder.Items[2] as TabItem;
            tab.IsEnabled = false;
            tab = tabOrder.Items[3] as TabItem;
            tab.IsEnabled = false;
            dpSale.SelectedDate = DateTime.Now;
            dpDueDate.SelectedDate = DateTime.Now.AddDays(5);

        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(call.Test.ToString());
            E_Serial it = new E_Serial
            {
                Line = GetLinenumber(),
                ProductID = ProductID,
                Product = cmbProduc.Text,
                Width = Convert.ToDecimal(call.txtWidth.Text + call.cmbWidth.SelectedValue.ToString().Substring(1,4)),
                Lenght = Convert.ToDecimal(call.txtLenght.Text +  call.cmbLenght.SelectedValue.ToString().Substring(1,4)),
                Fabric = call.cmbFabric.SelectedValue.ToString(),
                Lining = call.cmbLinig.Text,
                Mount = call.cmbMount.Text,
                Operation = call.cmbOperation.Text,
                Control = call.cmbControl.Text,
                CordPosition = call.cmbCord.Text,
                HeadRail = call.cmbheadrail.Text,
                Valance = call.cmbValance.Text,
                Banding = call.cmbBanding.Text,
                Room = call.txtRoom.Text,
                Cost = Convert.ToDecimal(call.txtCost.Text)
               
            };
            Items.Add(it);
            dgSumary.ItemsSource = Items;



            gridTemplate.Children.Clear();
            cmbProduc.ItemsSource = null;
            DataTable tdbType = objBusOrder.ProductType(100);
            cmbProduc.ItemsSource = tdbType.DefaultView;
        }

        private int GetLinenumber()
        {
            int Line = 0;
            if(Items.Count == 0)
            {
                return 1;
            }
            else
            {
                foreach(E_Serial it in Items)
                {
                    Line = it.Line;
                }
                return Line + 1;
            }
        }

        private void btnSaveOrder_Click(object sender, RoutedEventArgs e)
        {
            decimal FabricWidth = 0;
            decimal FabricLength = 0;
            decimal FinishLateralPanel = 0;

            string Orderid = objBusOrder.NewOrderNO(Convert.ToInt32(txtCustomerID.Text), txtPO.Text, dpPO.SelectedDate.Value.ToShortDateString());
            int Serialid = 0;
            foreach(E_Serial it in Items)
            {
                Serialid = objBusOrder.NewSerialID(Orderid, it.ProductID, it.Line, Convert.ToInt32(cmbShipvia.SelectedValue.ToString()),
                     dpSale.SelectedDate.Value.ToShortDateString(), dpDueDate.SelectedDate.Value.ToShortDateString(), it.Cost);


                #region Width
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 111,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Width.ToString()
                });
                #endregion

                #region Lenght
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 103,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Lenght.ToString()
                });
                
                #endregion

                #region Fabric
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 112,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Fabric
                });

                #endregion

                #region Lining
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 113,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Lining
                });


                #endregion

                #region Mount
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 102,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Mount
                });

                #endregion

                #region Operation
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 114,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Operation
                });

                #endregion

                #region Control
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 115,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Control
                });

                #endregion

                #region cord Position
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 116,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.CordPosition
                });

                #endregion

                #region HeadRail
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 119,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.HeadRail
                });

                #endregion

                #region valance
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 117,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Valance
                });

                #endregion

                #region Banding
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 118,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Banding
                });
                #endregion

                #region OrderSize
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 104,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Width.ToString() + " X " + it.Lenght.ToString()
                });
                #endregion

                #region Sidemark
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 105,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = txtSidemark.Text
                });
                #endregion

                #region order number
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 106,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = Orderid.ToString()
                });
                #endregion

                #region Product Description
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 107,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Product
                });
                #endregion

                #region Sale Date
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 109,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = dpSale.SelectedDate.Value.ToShortDateString()
                });
                #endregion

                #region Due Date
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 110,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = dpDueDate.SelectedDate.Value.ToShortDateString()
                });
                #endregion

                #region Room
                Items_Details.Add(new E_ProductionDetails()
                {
                    OptionID = 108,
                    OrderID = Orderid,
                    SerialID = Serialid,
                    Value = it.Room
                });
                #endregion



                foreach(E_ProductionDetails itpd in Items_Details)
                {
                    objBusOrder.BuildSeria(itpd.OrderID, itpd.SerialID, itpd.OptionID, itpd.Value);
                }

                Items_Details.Clear();

                #region Options to calculate
                DataTable tdbCalculateOption = objBusOrder.BuildSerialOptions(it.ProductID, Serialid);
                foreach(DataRow row in tdbCalculateOption.Rows)
                {
                    switch(Convert.ToInt32(row[1].ToString()))
                    {
                        case 100: //Headreal size
                            F_HRsize objhrsize = new F_HRsize();
                            Items_Details.Add(new E_ProductionDetails()
                            {
                                OptionID = 100,
                                OrderID = Orderid,
                                SerialID = Serialid,
                                Value = objhrsize.getHeadrailSize(it.Mount, it.Width).ToString()
                            });
                            break;
                        case 101: //Headreal Part
                            F_HeadrailPart objhrpart = new F_HeadrailPart();
                            Items_Details.Add(new E_ProductionDetails()
                            {
                                OptionID = 101,
                                OrderID = Orderid,
                                SerialID = Serialid,
                                Value = objhrpart.HeadrailPart(it.Operation)
                            });
                            break;
                       case 1124: //Finish Length
                            F_FinishLenght objflng = new F_FinishLenght();
                            Items_Details.Add(new E_ProductionDetails()
                            {
                                OptionID = 1124,
                                OrderID = Orderid,
                                SerialID = Serialid,
                                Value = objflng.getFinishLenght(it.Operation, it.Lenght).ToString()
                            });
                            break;
                       case 1125: //Finish Width
                            F_FinishWidth objfwdth = new F_FinishWidth();
                            Items_Details.Add(new E_ProductionDetails()
                            {
                                OptionID = 1125,
                                OrderID = Orderid,
                                SerialID = Serialid,
                                Value = objfwdth.getFinishWidth(it.HeadRail, it.Mount, it.Width, "").ToString()
                            });
                            break;
                       case 1122: //Fabric Width
                            F_FabricWidth objfacW = new F_FabricWidth();
                            FabricWidth = objfacW.FabricWidth(it.Width);
                            Items_Details.Add(new E_ProductionDetails()
                            {
                                OptionID = 1122,
                                OrderID = Orderid,
                                SerialID = Serialid,
                                Value = objfacW.FabricWidth(it.Width).ToString()
                            });
                            if(it.Width > 48)
                            {
                                F_PanelWidth objPanel = new F_PanelWidth(it.Width, objfacW.FabricWidth(it.Width));
                                FinishLateralPanel = objPanel.FinalPanelWidth;
                                Items_Details.Add(new E_ProductionDetails()
                                {
                                    OptionID = 1119, //Lateral Panel
                                    OrderID = Orderid,
                                    SerialID = Serialid,
                                    Value = objPanel.PanelWidth.ToString()
                                });
                                Items_Details.Add(new E_ProductionDetails()
                                {
                                    OptionID = 1126, //Finish Lateral Panel
                                    OrderID = Orderid,
                                    SerialID = Serialid,
                                    Value = objPanel.FinalPanelWidth.ToString()
                                });
                            }
                            break;
                       case 1127: //Fabric Length
                            F_FabricLength objFacL = new F_FabricLength(it.Width);
                            FabricLength = objFacL.FabricLength;
                            Items_Details.Add(new E_ProductionDetails()
                            {
                                OptionID = 1127,
                                OrderID = Orderid,
                                SerialID = Serialid,
                                Value = objFacL.FabricLength.ToString()
                            });
                            break;
                     




                    }

                    #region Lining 
                    if(it.Lining != "N/A")
                    {
                        F_Lining objLining = new F_Lining(it.Width, it.Lining, FabricWidth, FinishLateralPanel, FabricLength);
                        Items_Details.Add(new E_ProductionDetails()
                        {
                            OptionID = 1128, //Lining Width
                            OrderID = Orderid,
                            SerialID = Serialid,
                            Value = objLining.LiningWidth.ToString()
                        });
                        Items_Details.Add(new E_ProductionDetails()
                        {
                            OptionID = 1129, //Lateral Lining Width
                            OrderID = Orderid,
                            SerialID = Serialid,
                            Value = objLining.LateralLinig.ToString()
                        });
                        Items_Details.Add(new E_ProductionDetails()
                        {
                            OptionID = 1130, //Lining Length
                            OrderID = Orderid,
                            SerialID = Serialid,
                            Value = objLining.LiningLength.ToString()
                        });

                    }
                    #endregion


                }
                foreach (E_ProductionDetails itpd in Items_Details)
                {
                    objBusOrder.BuildSeria(itpd.OrderID, itpd.SerialID, itpd.OptionID, itpd.Value);
                }

                Items_Details.Clear();


                #endregion



            }

        }

        private void btnPOnext_Click(object sender, RoutedEventArgs e)
        {
            var tab = tabOrder.Items[3] as TabItem;
            tab.IsEnabled = true;
            tab = tabOrder.Items[2] as TabItem;
            tab.IsEnabled = false;
        }

        
    }
}
