﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataOrderEntry
{
    public class E_ProductionDetails
    {
        public int OptionID { get; set; }
        public int SerialID { get; set; }
        public string OrderID { get; set; }
        public string Value { get; set; }
    }
}
