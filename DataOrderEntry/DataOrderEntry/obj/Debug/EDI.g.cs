﻿#pragma checksum "..\..\EDI.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "57EEDA109216521319F4A427A292CF6491151DF3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BolapanControl.ItemsFilter;
using BolapanControl.ItemsFilter.Initializer;
using BolapanControl.ItemsFilter.Model;
using BolapanControl.ItemsFilter.View;
using BolapanControl.ItemsFilter.ViewModel;
using ExtendedGrid.ExtendedGridControl;
using Microsoft.Windows.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DataOrderEntry {
    
    
    /// <summary>
    /// EDI
    /// </summary>
    public partial class EDI : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tabEDI;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dg850Orders;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txt850;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn850Convert;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn850Import;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnOrder850;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrint850;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dg850Print;
        
        #line default
        #line hidden
        
        
        #line 154 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn855toCreate;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dgPrint855;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtPrintEDI855;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrintAnsi855;
        
        #line default
        #line hidden
        
        
        #line 163 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn810Export855;
        
        #line default
        #line hidden
        
        
        #line 215 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn856toCreate;
        
        #line default
        #line hidden
        
        
        #line 218 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dgPrint856;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtPrintEDI856;
        
        #line default
        #line hidden
        
        
        #line 224 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrintAnsi856;
        
        #line default
        #line hidden
        
        
        #line 225 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn810Export856;
        
        #line default
        #line hidden
        
        
        #line 279 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn810toCreate;
        
        #line default
        #line hidden
        
        
        #line 282 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dgPO;
        
        #line default
        #line hidden
        
        
        #line 286 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnGenerate;
        
        #line default
        #line hidden
        
        
        #line 287 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txt810;
        
        #line default
        #line hidden
        
        
        #line 293 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem tab810Print;
        
        #line default
        #line hidden
        
        
        #line 343 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dgPOPrint;
        
        #line default
        #line hidden
        
        
        #line 346 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock txtPrintEDI;
        
        #line default
        #line hidden
        
        
        #line 348 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnPrintAnsi;
        
        #line default
        #line hidden
        
        
        #line 349 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btn810Export;
        
        #line default
        #line hidden
        
        
        #line 380 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbProductGroup;
        
        #line default
        #line hidden
        
        
        #line 382 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cmbProduc;
        
        #line default
        #line hidden
        
        
        #line 384 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSave;
        
        #line default
        #line hidden
        
        
        #line 387 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridTemplate;
        
        #line default
        #line hidden
        
        
        #line 416 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpSale;
        
        #line default
        #line hidden
        
        
        #line 417 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DatePicker dpDueDate;
        
        #line default
        #line hidden
        
        
        #line 418 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSaveOrder;
        
        #line default
        #line hidden
        
        
        #line 420 "..\..\EDI.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal BolapanControl.ItemsFilter.FilterDataGrid dgSumary;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DataOrderEntry;component/edi.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\EDI.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 9 "..\..\EDI.xaml"
            ((DataOrderEntry.EDI)(target)).Loaded += new System.Windows.RoutedEventHandler(this.UserControl_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tabEDI = ((System.Windows.Controls.TabControl)(target));
            return;
            case 3:
            this.dg850Orders = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            return;
            case 4:
            this.txt850 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.btn850Convert = ((System.Windows.Controls.Button)(target));
            
            #line 67 "..\..\EDI.xaml"
            this.btn850Convert.Click += new System.Windows.RoutedEventHandler(this.btn850Convert_Click);
            
            #line default
            #line hidden
            return;
            case 6:
            this.btn850Import = ((System.Windows.Controls.Button)(target));
            
            #line 68 "..\..\EDI.xaml"
            this.btn850Import.Click += new System.Windows.RoutedEventHandler(this.btn850Import_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 72 "..\..\EDI.xaml"
            ((System.Windows.Controls.TabItem)(target)).Loaded += new System.Windows.RoutedEventHandler(this.TabItem_Loaded);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnOrder850 = ((System.Windows.Controls.Button)(target));
            
            #line 96 "..\..\EDI.xaml"
            this.btnOrder850.Click += new System.Windows.RoutedEventHandler(this.btnOrder850_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.btnPrint850 = ((System.Windows.Controls.Button)(target));
            return;
            case 10:
            this.dg850Print = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            return;
            case 11:
            this.btn855toCreate = ((System.Windows.Controls.Button)(target));
            
            #line 154 "..\..\EDI.xaml"
            this.btn855toCreate.Click += new System.Windows.RoutedEventHandler(this.btn855toCreate_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.dgPrint855 = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            return;
            case 13:
            this.txtPrintEDI855 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.btnPrintAnsi855 = ((System.Windows.Controls.Button)(target));
            
            #line 162 "..\..\EDI.xaml"
            this.btnPrintAnsi855.Click += new System.Windows.RoutedEventHandler(this.btnPrintAnsi855_Click);
            
            #line default
            #line hidden
            return;
            case 15:
            this.btn810Export855 = ((System.Windows.Controls.Button)(target));
            return;
            case 16:
            this.btn856toCreate = ((System.Windows.Controls.Button)(target));
            
            #line 215 "..\..\EDI.xaml"
            this.btn856toCreate.Click += new System.Windows.RoutedEventHandler(this.btn856toCreate_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.dgPrint856 = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            return;
            case 18:
            this.txtPrintEDI856 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.btnPrintAnsi856 = ((System.Windows.Controls.Button)(target));
            
            #line 224 "..\..\EDI.xaml"
            this.btnPrintAnsi856.Click += new System.Windows.RoutedEventHandler(this.btnPrintAnsi856_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.btn810Export856 = ((System.Windows.Controls.Button)(target));
            return;
            case 21:
            this.btn810toCreate = ((System.Windows.Controls.Button)(target));
            
            #line 279 "..\..\EDI.xaml"
            this.btn810toCreate.Click += new System.Windows.RoutedEventHandler(this.btn810toCreate_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.dgPO = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            
            #line 283 "..\..\EDI.xaml"
            this.dgPO.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.dgPO_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 23:
            this.btnGenerate = ((System.Windows.Controls.Button)(target));
            
            #line 286 "..\..\EDI.xaml"
            this.btnGenerate.Click += new System.Windows.RoutedEventHandler(this.btnGenerate_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.txt810 = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 25:
            this.tab810Print = ((System.Windows.Controls.TabItem)(target));
            
            #line 293 "..\..\EDI.xaml"
            this.tab810Print.Loaded += new System.Windows.RoutedEventHandler(this.tab810Print_Loaded);
            
            #line default
            #line hidden
            return;
            case 26:
            this.dgPOPrint = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            return;
            case 27:
            this.txtPrintEDI = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 28:
            this.btnPrintAnsi = ((System.Windows.Controls.Button)(target));
            
            #line 348 "..\..\EDI.xaml"
            this.btnPrintAnsi.Click += new System.Windows.RoutedEventHandler(this.btnPrintAnsi_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.btn810Export = ((System.Windows.Controls.Button)(target));
            
            #line 349 "..\..\EDI.xaml"
            this.btn810Export.Click += new System.Windows.RoutedEventHandler(this.btn810Export_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.cmbProductGroup = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 31:
            this.cmbProduc = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 32:
            this.btnSave = ((System.Windows.Controls.Button)(target));
            return;
            case 33:
            this.gridTemplate = ((System.Windows.Controls.Grid)(target));
            return;
            case 34:
            this.dpSale = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 35:
            this.dpDueDate = ((System.Windows.Controls.DatePicker)(target));
            return;
            case 36:
            this.btnSaveOrder = ((System.Windows.Controls.Button)(target));
            return;
            case 37:
            this.dgSumary = ((BolapanControl.ItemsFilter.FilterDataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

