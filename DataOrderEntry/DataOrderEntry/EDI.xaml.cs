﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Entity;
using Business;
using System.Data;
using System.IO;
using Microsoft.Win32;

namespace DataOrderEntry
{
    /// <summary>
    /// Interaction logic for EDI.xaml
    /// </summary>
    public partial class EDI : UserControl
    {
        B_Order objBus = new B_Order();
        B_EDI objBusEdi = new B_EDI();
        List<E_Ordernot810> Items = new List<E_Ordernot810>();
        List<E_EDISeg> EDI_Items = new List<E_EDISeg>();
        List<E_EDIPrint> EDI_ItemsPrint = new List<E_EDIPrint>();
        List<E_850DataOrder> EDI_850Order = new List<E_850DataOrder>();
        List<E_EDIProdctionDetails> EDI_ProdDetails = new List<E_EDIProdctionDetails>();
        List<E_EDIOrder> EDI_Order = new List<E_EDIOrder>();
        List<E_OrdertoPrint> EDI_OrdertoPrint = new List<E_OrdertoPrint>();
        List<E_EDIOrders> EDI_855List = new List<E_EDIOrders>();
        List<E_EDIOrders> EDI_856List = new List<E_EDIOrders>();
        List<E_810toCreate> EDI_810List = new List<E_810toCreate>();

        string HomeDepotPO = "";
        //Var to store the total cost of the order
        decimal TotalCost = 0;
       

        public EDI()
        {
            InitializeComponent();
        }

        private void EDIPrint(int DocumentID)
        {
            DataTable tdb = objBus.EDIPrint(DocumentID);
            foreach(DataRow rw in tdb.Rows)
            {
                E_EDIPrint it = new E_EDIPrint
                {
                 PO = rw[0].ToString(),
                 BWS_Invoice = rw[1].ToString(),
                 PO_Date = Convert.ToDateTime(rw[2].ToString()).ToShortDateString(),
                 CustomerName = rw[3].ToString()
                };
                EDI_ItemsPrint.Add(it);
            }
            dgPOPrint.ItemsSource = EDI_ItemsPrint;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        private string CreateL3Invoice(string PO)
        {
            return objBusEdi.CreateL3invoice(PO);
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            /*
            if(txtTracking.Text != string.Empty && txtInvoice.Text != string.Empty && dpInvDate.SelectedDate != null)
            {
                UpdateBWSData();
                EDIElements(810);
                CreateANSI();
               
            }
            else
            {
                MessageBox.Show("POR FAVOR VERIFIQUE SI FALTA ALGUNA INFORMACION", "ERROR - FALTA DE INFORMACION", MessageBoxButton.OK, MessageBoxImage.Error);
            }*/
            string L3Inv = "";

            if (dgPO.SelectedItems.Count > 0)
            {
                E_810toCreate item = (E_810toCreate) dgPO.SelectedItems[0];
                //Getting all segments for 810
                EDIElements(810);
                //Create Invoice Number
                L3Inv = CreateL3Invoice(item.PONumber);
                //Create Ansi
                CreateANSI810(item.PONumber, L3Inv);

                    //export to txt
                    string ANSI = txt810.Text;
                    string TextLine = "";
                    // Set a variable to the My Documents path.
                    string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                    foreach (char C in ANSI)
                    {
                        TextLine += C;
                        if (C == Convert.ToChar("~"))
                        {
                            //  TextLine += Environment.NewLine;
                        }
                    }
                    // Write the text to a new file named "WriteFile.txt".
                    string txtname = "810 Invoice - PO Number " + item.PONumber + " " + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                    File.WriteAllText(System.IO.Path.Combine(mydocpath, txtname), TextLine);
                    TextLine = string.Empty;

                    objBus.CreateANSI(810, item.PONumber, txt810.Text);
                    MessageBox.Show("CODIGO ANSI GUADADO CORRECTAMENTE!!", "SAVE ANSI", MessageBoxButton.OK, MessageBoxImage.Information);
                    EDI_810List.Clear();
             }
            else
            {
                MessageBox.Show("Por favor seleciona una orden", "NO ORDER SELECTED", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void CreateANSI810(string PO, string L3Invoice)
        {
            int CountSegments = 0;
            string TempDate = "";
            string LineAnsi = "";
            int Temp = EDI_Items[0].edsID;
            E_810toCreate item = (E_810toCreate)dgPO.SelectedItems[0];
            DataTable tdb810Lines = objBusEdi.getLineswithDetail(PO);
            List<E_EDISeg> EDISegLines810 = new List<E_EDISeg>();
            Boolean Flag_Lines = false;

            foreach (E_EDISeg it in EDI_Items)
            {
                if (it.edtsID >= 461 && it.edtsID < 470) //Ruben: 377, 386 ** BWS: 461, 470
                {
                    EDISegLines810.Add(it);
                }
                else if ((it.edtsID == 470)) //Ruben: 386 ** BWS:  470
                {
                    EDISegLines810.Add(it);
                    LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                    LineAnsi += "~";
                    txt810.Text += LineAnsi + Environment.NewLine;
                    LineAnsi = string.Empty;
                    CountSegments++;

                    CountSegments += CreateANSI810Lines(EDISegLines810, tdb810Lines);
                    Flag_Lines = true;
                    //  Temp = it.edsID;
                }
                else
                {
                    if (Flag_Lines == true)
                    {
                        Temp = it.edsID;
                        Flag_Lines = false;
                    }

                    //New Line
                    if (it.edsID != Temp)
                    {
                        LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                        LineAnsi += "~";
                        txt810.Text += LineAnsi + Environment.NewLine;
                        LineAnsi = string.Empty;
                        CountSegments++;
                    }

                    //if it's defaul value from Layout
                    if (it.StaticFlag == 1)
                    {
                        LineAnsi += it.Value + "*";
                    }
                    else if (it.edtsID == 416) //ISA DATE, Ruben: 332 ** BWS:416
                    {
                        TempDate = DateTime.Now.ToString("yyMMdd");
                        LineAnsi += TempDate + "*";
                    }
                    else if (it.edtsID == 428) //GS DATE, Ruben: 344 ** BWS:428
                    {
                        TempDate = DateTime.Now.ToString("yyyyMMdd");
                        LineAnsi += TempDate + "*";
                    }
                    else if (it.edtsID == 417 || it.edtsID == 429) //ISA AND GS TIME, Ruben: 333, 345 ** BWS:417, 429
                    {
                        TempDate = DateTime.Now.ToString("HHmm");
                        LineAnsi += TempDate + "*";
                    }
                    else if (it.edtsID == 457) //Order Date, Ruben: 373 ** BWS:457
                    {
                        LineAnsi += Convert.ToDateTime(item.PODate).ToString("yyyyMMdd") + "*";
                    }
                    else if (it.Query.Contains("Total")) //Total cost of the order
                    {
                        string Amount = TotalCost.ToString();
                        Amount = Amount.Replace(".", "");
                        LineAnsi += Amount + "*";
                    }
                    else if (it.Query.Contains("Items")) //CTT Qty lines
                    {
                        LineAnsi += tdb810Lines.Rows.Count.ToString() + "*";
                    }
                    else if (it.Query.Contains("Count")) //Qty Segments
                    {
                        LineAnsi += (CountSegments - 1).ToString() + "*"; 
                    }
                    else if (it.Query.Contains("Grid"))
                    {
                        #region Grid
                        int Element = GetElementNumber(it.Query);

                        switch (Element)
                        {
                            case 2:
                                LineAnsi += Convert.ToDateTime(item.PODate).ToString("yyyyMMdd") + "*";
                                break;
                            case 1:
                                LineAnsi += item.PONumber + "*";
                                break;
                            case 8:
                                if (item.AffiliateID == "03") //Binds.com
                                {
                                    LineAnsi += "BC2014000" + "*";
                                }
                                else
                                {
                                    LineAnsi += "AM2014001" + "*";
                                }
                                break;
                            case 3:
                                LineAnsi += item.Customer + "*";
                                break;
                            case 4:
                                LineAnsi += item.Address + "*";
                                break;
                            case 5:
                                LineAnsi += item.City + "*";
                                break;
                            case 6:
                                LineAnsi += item.State + "*";
                                break;
                            case 7:
                                LineAnsi += item.PostalCode + "*";
                                break;
                            case 10:
                                LineAnsi += Convert.ToDateTime(item.ShipOn).ToString("yyyyMMdd") + "*";
                                break;
                            case 9:
                                LineAnsi += item.Tracking + "*";
                                break;
                        }
                        #endregion

                    }
                    else if (it.Query != "")
                    {
                        #region Query
                        if (it.edtsID == 420 || it.edtsID == 489 || it.edtsID == 492) //ISA CONTROL SOFTWARE , Ruben: 336, 405, 408 ** BWS:420, 489, 492
                        {
                            LineAnsi += CreateISAnumber(objBus.EDIQuery(it.Query, PO, 0)) + "*"; //Quitar el 1 cuando este con Cesar
                        }
                        else if (it.edtsID == 437) //Invoice Date , Ruben: 353 ** BWS:437
                        {
                            LineAnsi += Convert.ToDateTime(objBus.EDIQuery(it.Query, L3Invoice, 1)).ToString("yyyyMMdd") + "*";
                        }
                        else if (it.edtsID == 438) //Invoice Number , Ruben: 354 ** BWS:438
                        {
                            LineAnsi += L3Invoice + "*";
                        }
                        else if (it.edtsID == 430) //GS Control Software Number, Ruben: 346 ** BWS:430
                        {
                            //QUITAR UN CERO CUANDO ENTRE CON CESAR
                            LineAnsi += "000" + objBus.EDIQuery(it.Query, PO, 0) + "*";
                        }
                        else if (it.edtsID == 435 || it.edtsID == 486) //ST  and SE Control Software Number, Ruben: 351, 402 ** BWS:435, 486 
                        {
                            //QUITAR UN CERO CUANDO ENTRE CON CESAR
                            LineAnsi += "00000" + objBus.EDIQuery(it.Query, PO, 0) + "*";
                        }
                        #endregion
                    }
                }

                Temp = it.edsID;
            }

            LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
            LineAnsi += "~";
            txt810.Text += LineAnsi;
            LineAnsi = string.Empty;

        }

        private int CreateANSI810Lines(List<E_EDISeg> LIst, DataTable tdb)
        {
           int CountSegments = 0;
             string LineAnsi = "";
            int Temp = LIst[0].edsID;
            int CountLInes = 2;
            decimal Cost = 0;
            int Quantity = 0;
            
            foreach (DataRow dr in tdb.Rows)
            {
                 CountLInes++;
                 foreach (E_EDISeg it in LIst)
                 {
                     
                     //if it's defaul value from Layout
                     if (it.StaticFlag == 1)
                     {
                         LineAnsi += it.Value + "*";

                     }
                     else if (it.Query.Contains("Grid"))
                     {
                         int Element = GetElementNumber(it.Query);
                         if(Element == 3) //Get Cost
                         {
                            // Cost = Convert.ToDecimal(dr[Element].ToString());
                         }
                         if(Element == 4) //Get Quantity
                         {
                             Quantity = Convert.ToInt16(dr[Element].ToString());
                         }

                         LineAnsi += dr[Element].ToString() + "*";
                     }
                     else if (it.Query != "")
                     {
                         if (it.edtsID == 468) //Ruben: 384 ** BWS:468
                         {
                             string Line = dr[2].ToString(); //Line Number
                             LineAnsi += objBus.EDIQuery(it.Query, Line, 1) + "*";
                         }
                         if(it.edtsID == 465) //Ruben: 381 ** BWS:465
                         {
                              string Line = dr[2].ToString(); //Line Number
                              LineAnsi += objBus.EDIQuery(it.Query, Line, 1) + "*";
                              Cost = Convert.ToDecimal(objBus.EDIQuery(it.Query, Line, 1));
                         }
                     }

                     Temp = it.edsID;
                 }

                 //New Line
                     LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                     LineAnsi += "~";
                     txt810.Text += LineAnsi + Environment.NewLine;
                     LineAnsi = string.Empty;
                     CountSegments++;
                     //Getting the cost of blinds
                     TotalCost += Cost * Quantity;
                     Cost = 0;
                     Quantity = 0;
            }
           // LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
           // LineAnsi += "~";
           // txt810.Text += LineAnsi + Environment.NewLine;
           // LineAnsi = string.Empty;
           // CountSegments++;

            return CountSegments;

        }

        private void CreateANSI()
        {
            /*
            string LineAnsi = "";
            int Temp = 9;
            string TempDate = "";
            foreach(E_EDISeg it in EDI_Items)
            {
                if (it.edsID != Temp)
                {
                    LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                    LineAnsi += "~";
                    txtText.Text += LineAnsi + Environment.NewLine;
                    LineAnsi = string.Empty;
                }
                if(it.StaticFlag == 1)
                {
                    LineAnsi += it.Value + "*";
                }
                else if(it.edtsID == 5) //HomeDepot PO Number
                {
                    LineAnsi += HomeDepotPO + "*";
                }
                else if(it.Query != "")
                {
                    if (it.edtsID == 2 || it.edtsID == 4)
                    {
                        TempDate = Convert.ToDateTime(objBus.EDIQuery(it.Query, HomeDepotPO, 1)).ToString("yyyyMMdd");
                        LineAnsi += TempDate + "*";
                    }
                    else if(it.edtsID == 58 || it.edtsID == 63)
                    {
                        LineAnsi += objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*";
                    }
                    else
                    {
                        LineAnsi += objBus.EDIQuery(it.Query, HomeDepotPO, 1) + "*";
                    }
                }
                else if(it.edtsID == 6) //BWS ID with HOMEDEPOT
                {
                    LineAnsi += "BLINDS.COM=3*";
                }
                else
                {
                    LineAnsi += "*";
                }
                
              
                Temp = it.edsID;
            }
             * */
        }

        private void UpdateBWSData()
        {
            //objBus.EDI_UpdateBWSData(txtInvoice.Text, txtTracking.Text, dpInvDate.SelectedDate.Value.ToShortDateString(), HomeDepotPO);
        }

        private void dgPO_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (dgPO.SelectedItems.Count > 0)
            {
                E_810toCreate ticket = (E_810toCreate)dgPO.SelectedItems[0];
                HomeDepotPO = ticket.PONumber;
               
            }
    
        }

        private void EDIElements(int Documents)
        {
            DataTable tdb = objBus.EDIElements(Documents);
            foreach(DataRow rw in tdb.Rows)
            {
                E_EDISeg it = new E_EDISeg
                {
                    edtsID = Convert.ToInt32(rw[0].ToString()),
                    edsID = Convert.ToInt32(rw[1].ToString()),
                    edID = Convert.ToInt32(rw[2].ToString()),
                    SegID = rw[3].ToString(),
                    SegName = rw[4].ToString(),
                    Element = Convert.ToInt32(rw[5].ToString()),
                    Name = rw[6].ToString(),
                    Value = rw[7].ToString(),
                    StaticFlag = Convert.ToInt32(rw[8].ToString()),
                    Query = rw[9].ToString()

                };
                EDI_Items.Add(it);
            }
        }

        private void tab810Print_Loaded(object sender, RoutedEventArgs e)
        {
            EDIPrint(810);
        }

        private void btnPrintAnsi_Click(object sender, RoutedEventArgs e)
        {
            string ANSI = "";
            for(int i = 0; i<dgPOPrint.Items.Count - 1; i++)
            {
                E_EDIPrint it = (E_EDIPrint)dgPOPrint.Items[i];
                ANSI = objBus.GettingANSI(810, it.PO);
                foreach(char C in ANSI)
                {
                    txtPrintEDI.Text += C;
                    if(C == Convert.ToChar("~"))
                    {
                       // txtPrintEDI.Text += Environment.NewLine;
                    }
                }
            }
        }

        private void btn810Export_Click(object sender, RoutedEventArgs e)
        {
              string ANSI = txtPrintEDI.Text;
              string TextLine = "";
              // Set a variable to the My Documents path.
              string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            foreach(char C in ANSI)
            {
                TextLine += C;
                if (C == Convert.ToChar("~"))
                {
                  //  TextLine += Environment.NewLine;
               }
            }
            // Write the text to a new file named "WriteFile.txt".
            string txtname = "810Invoice" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            File.WriteAllText(System.IO.Path.Combine(mydocpath, txtname), TextLine);
            TextLine = string.Empty;
        }

        private void btn850Import_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                txt850.Text = File.ReadAllText(openFileDialog.FileName);
        }

        private void ReadText850()
        {
            string Line = "";
            string Seg = "";
            DataTable tdb = new DataTable();
            tdb = null;
            foreach(char c in txt850.Text)
            {
                Line += c;
                if(c == Convert.ToChar("*"))
                {
                    if (tdb == null)
                    {
                        Seg = Line.Substring(0, Line.Length-1);
                        tdb = objBus.EDI850Segments(Seg);
                    }
                }
                if (c == Convert.ToChar("~"))
                {
                    Seg = Seg.Replace("\n", "");
                    Seg = Seg.Replace("\r", "");
                    Line = Line.Replace("\n", "");
                    Line = Line.Replace("\r", "");
                    Read850Line(Seg, Line);
                    Seg = string.Empty;
                    Line = string.Empty;
                    tdb = null;
                }
                
            }
        }

        private void Read850Line(string SegmentID, string Line)
        {
            DataTable tdb = objBus.EDI850Segments(SegmentID);
            string Value = "";
            int Count = 0;

            switch(SegmentID)
            {
                case "BEG":
                    #region BEG
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;
                                             
                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "REF":
                    #region REF
                    if (ExistSeg(25) == false)
                    {

                        foreach (char c in Line)
                        {
                            Value += c;
                            if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                            {
                                Value = Value.Substring(0, Value.Length - 1);
                                E_850DataOrder it = new E_850DataOrder
                                {
                                    edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                    edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                    edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                    Value = Value,
                                    Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                                };
                                EDI_850Order.Add(it);
                                Count++;

                                Value = "";
                            }
                        }
                    }
                    else
                    {
                        Count = 3; //Porque existe dos veces el Segmento REF
                        foreach (char c in Line)
                        {
                            Value += c;
                            if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                            {
                                Value = Value.Substring(0, Value.Length - 1);
                                E_850DataOrder it = new E_850DataOrder
                                {
                                    edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                    edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                    edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                    Value = Value,
                                    Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                                };
                                EDI_850Order.Add(it);
                                Count++;

                                Value = "";
                            }
                        }
                    }
#endregion

                    break;
                case "DTM":
                    #region DTM
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "TD5":
                    #region TD5
                    if (ExistSeg(28) == false)
                    {

                        foreach (char c in Line)
                        {
                            Value += c;
                            if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                            {
                                Value = Value.Substring(0, Value.Length - 1);
                                E_850DataOrder it = new E_850DataOrder
                                {
                                    edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                    edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                    edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                    Value = Value,
                                    Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                                };
                                EDI_850Order.Add(it);
                                Count++;

                                Value = "";
                            }
                        }
                    }
                    else
                    {
                        Count = 3; //Porque existe dos veces el Segmento TD5
                        E_850DataOrder it = new E_850DataOrder
                        {
                            edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                            edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                            edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                            Value = Line.Substring(0, 3),
                            Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                        };
                        EDI_850Order.Add(it);

                        Count++;
                        E_850DataOrder it1 = new E_850DataOrder
                        {
                            edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                            edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                            edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                            Value = Line.Substring(17, 2),
                            Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                        };
                        EDI_850Order.Add(it1);
                    }

                    #endregion
                    break;
                case "N1":
                    #region N1
                    if (ExistSeg(29) == false)
                    {

                        foreach (char c in Line)
                        {
                            Value += c;
                            if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                            {
                                Value = Value.Substring(0, Value.Length - 1);
                                E_850DataOrder it = new E_850DataOrder
                                {
                                    edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                    edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                    edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                    Value = Value,
                                    Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                                };
                                EDI_850Order.Add(it);
                                Count++;

                                Value = "";
                            }
                        }
                    }
                    else
                    {
                        Count = 3; //Porque existe dos veces el Segmento N1
                        foreach (char c in Line)
                        {
                            Value += c;
                            if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                            {
                                Value = Value.Substring(0, Value.Length - 1);
                                E_850DataOrder it = new E_850DataOrder
                                {
                                    edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                    edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                    edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                    Value = Value,
                                    Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                                };
                                EDI_850Order.Add(it);
                                Count++;

                                Value = "";
                            }
                        }
                    }
                    #endregion
                    break;
                case "N2":
                    #region N2
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "N3":
                    #region N3
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "N4":
                    #region N4
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "PO1":
                    #region P01
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "CN1":
                    #region CN1
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "PID":
                    #region CN1
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;
                case "CTT":
                    #region CN1
                    foreach (char c in Line)
                    {
                        Value += c;
                        if (c == Convert.ToChar("*") || c == Convert.ToChar("~"))
                        {
                            Value = Value.Substring(0, Value.Length - 1);
                            E_850DataOrder it = new E_850DataOrder
                            {
                                edstID = Convert.ToInt32(tdb.Rows[Count][0].ToString()),
                                edsID = Convert.ToInt32(tdb.Rows[Count][1].ToString()),
                                edID = Convert.ToInt32(tdb.Rows[Count][2].ToString()),
                                Value = Value,
                                Element = Convert.ToInt32(tdb.Rows[Count][3].ToString())
                            };
                            EDI_850Order.Add(it);
                            Count++;

                            Value = "";
                        }
                    }
                    #endregion
                    break;






            }
        }

        private void btn850Convert_Click(object sender, RoutedEventArgs e)
        {
           
                ReadText850();
                //dg850Orders.ItemsSource = EDI_850Order;
                //Get po number
                var Result = EDI_850Order.Where(d => d.edstID == 67).FirstOrDefault();
                string POnumber = Result.Value;
                if (objBusEdi.OrderExists(POnumber) == 0)
                {
                    Save850Data();
                    
                }
                else
                {
                    MessageBox.Show("Esta orden ya existe", "Orden duplicada", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                EDI_850Order.Clear();
        }

        private void Save850Data()
        {
            string BWSorderNo ="";
            int Line = 0;
            decimal Cost = 0;
            int QtyOrdered = 0;

            #region Crear order
            //CreateOrder BWS
            //Get po number
            var Result = EDI_850Order.Where(d => d.edstID == 67).FirstOrDefault();
            string POnumber = Result.Value;
            Result = EDI_850Order.Where(d => d.edstID == 68).FirstOrDefault();
            string POdate = Result.Value;
            Result = EDI_850Order.Where(d => d.edstID == 69).FirstOrDefault();
            string POcustomer = Result.Value;
            Result = EDI_850Order.Where(d => d.edstID == 94).FirstOrDefault();
            string POAddress = Result.Value;
            Result = EDI_850Order.Where(d => d.edstID == 97).FirstOrDefault();
            string POcity = Result.Value;
            Result = EDI_850Order.Where(d => d.edstID == 98).FirstOrDefault();
            string POstate = Result.Value;
            Result = EDI_850Order.Where(d => d.edstID == 99).FirstOrDefault();
            string POpostalcode = Result.Value.ToString();
   
            BWSorderNo = objBus.NewOrderNO(1006, POnumber, POdate, POcustomer, POAddress, POcity, POstate, POpostalcode);
            #endregion

            #region Save Data on table ediOrder
            //Save Data on table ediOrder
            Boolean Flag_Header = false;
            foreach (E_850DataOrder it in EDI_850Order)
            {
                if(it.Value == "PO1")
                {
                    Flag_Header = true;
                    Line = 0;
                }

                if (Flag_Header == false)
                {
                    objBus.EDIcreateOrder(it.edstID, it.edsID, it.edID, it.Value, it.Element, POnumber, 0);
                }
                else
                {
                    if(it.edstID == 101)
                    {
                        Line = Convert.ToInt32(it.Value);
                    }
                    objBus.EDIcreateOrder(it.edstID, it.edsID, it.edID, it.Value, it.Element, POnumber, Line);
                }

            }
            #endregion

            DataTable tdbLines = objBusEdi.getLines(POnumber);

            foreach(DataRow rw in tdbLines.Rows)
            {                                                           //Line Number
                DataTable tdbSerial = objBus.getSerialHeader(POnumber, Convert.ToInt32(rw[4].ToString()));
                foreach (DataRow dr in tdbSerial.Rows)
                {
                    if (Convert.ToInt16(dr[1].ToString()) == 110) //To get the cost
                    {
                        Cost = Convert.ToDecimal(dr[4].ToString());

                    }
                    if (Convert.ToInt16(dr[1].ToString()) == 102) //To get the Qty Ordered
                    {
                        QtyOrdered = Convert.ToInt16(dr[4].ToString());

                    }
                }
                //LineNumber - SAVE Data on Table tdbEdiProductionDetails
                SerialDetails850(POnumber, Convert.ToInt32(rw[4].ToString()));
                for (int i = 0; i < QtyOrdered; i++)
                {
                    //Line Number
                    int SerialID = objBus.NewSerialID(BWSorderNo, 9999, Convert.ToInt32(rw[4].ToString()), 1, DateTime.Now.ToShortDateString(), DateTime.Now.AddDays(5).ToShortDateString(), Cost);
                }

            }


           
            OrderReport(POnumber);
            txt850.Text = string.Empty;


        }

        private void SerialDetails850(string PO, int Line)
        {
            DataTable tdbDetails = objBusEdi.getSerialDetails(PO, Line);
            int OptionID = 0;
            string OptionName = "";
            string Value = "";
            int Index = 0;
            int Namelenght = 0;

            foreach(DataRow dr in tdbDetails.Rows)
            {
                OptionName = dr[4].ToString();
                Index = OptionName.IndexOf("=");
                Namelenght = OptionName.Length;
                Value = OptionName.Substring(Index + 1, Namelenght - 1 - Index);
                OptionName = OptionName.Substring(0, Index);

                OptionID = objBusEdi.getOptionID(OptionName);

                E_EDIProdctionDetails IT = new E_EDIProdctionDetails
                {
                    Line = Line,
                    OptionId = OptionID,
                    Value = Value
                };
                EDI_ProdDetails.Add(IT);


            }

            //Saving Serial Details on Data Base
            foreach (E_EDIProdctionDetails item in EDI_ProdDetails)
            {
                objBusEdi.SaveSerialDetails(item.OptionId, item.Value, item.Line);
            }

         //   dg850Orders.ItemsSource = EDI_ProdDetails;

        }

        private void OrderReport(string PONumber)
        {
            DataTable tdb = objBusEdi.OrderReport(PONumber);

            foreach(DataRow dr in tdb.Rows)
            {
                E_EDIOrder it = new E_EDIOrder
                {
                    BWSOrder = dr[0].ToString(),
                    POnumber = dr[1].ToString(),
                    PODate = Convert.ToDateTime(dr[2].ToString()),
                    Customer = dr[3].ToString(),
                    Line = Convert.ToInt32(dr[4].ToString()),
                    Product = dr[5].ToString(),
                    OperationSystem = dr[6].ToString(),
                    Cost = Convert.ToDecimal(dr[7].ToString())
                };
                EDI_Order.Add(it);
            }

            dg850Orders.ItemsSource = EDI_Order;
        }

        private bool ExistSeg(int SegNo)
        {
            var Result = EDI_850Order.Where(d => d.edsID == SegNo).FirstOrDefault();
            if(Result == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void TabItem_Loaded(object sender, RoutedEventArgs e)
        {
           
        }

        private void btnOrder850_Click(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBusEdi.OrderstoPrint();

            foreach(DataRow dr in tdb.Rows)
            {
                E_OrdertoPrint it = new E_OrdertoPrint
                {
                    BWSOrder = dr[0].ToString(),
                    POnumber = dr[1].ToString(),
                    POdate = Convert.ToDateTime(dr[2].ToString()).ToShortDateString(),
                    Customer = dr[3].ToString(),
                    Line = Convert.ToInt32(dr[4].ToString()),
                    Product = dr[5].ToString(),
                    OperationSystem = dr[6].ToString(),
                    Cost = Convert.ToDecimal(dr[7].ToString()),
                    Address = dr[8].ToString(),
                    City = dr[9].ToString(),
                    State = dr[10].ToString(),
                    PostalCode = Convert.ToInt32(dr[11].ToString())
                };
                EDI_OrdertoPrint.Add(it);
            }

            dg850Print.ItemsSource = EDI_OrdertoPrint;
        }

        private void btn855toCreate_Click(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBusEdi.get855toCreate();

            foreach(DataRow dr in tdb.Rows)
            {
                E_EDIOrders it = new E_EDIOrders
                {
                    BWSOrder = dr[0].ToString(),
                    POnumber = dr[1].ToString(),
                    PODate = Convert.ToDateTime(dr[2].ToString()).ToShortDateString(),
                    Customer = dr[3].ToString(),
                    Address = dr[4].ToString(),
                    City = dr[5].ToString(),
                    State = dr[6].ToString(),
                    PostalCode = dr[7].ToString(),
                };
                EDI_855List.Add(it);
            }

            dgPrint855.ItemsSource = EDI_855List;
        }

        private void dg855Ordertocreaterefresh()
        {
            DataTable tdb = objBusEdi.get855toCreate();

            foreach (DataRow dr in tdb.Rows)
            {
                E_EDIOrders it = new E_EDIOrders
                {
                    BWSOrder = dr[0].ToString(),
                    POnumber = dr[1].ToString(),
                    PODate = Convert.ToDateTime(dr[2].ToString()).ToShortDateString(),
                    Customer = dr[3].ToString(),
                    Address = dr[4].ToString(),
                    City = dr[5].ToString(),
                    State = dr[6].ToString(),
                    PostalCode = dr[7].ToString(),
                };
                EDI_855List.Add(it);
            }
            dg850Print.ItemsSource = null;
            dg850Print.Items.Refresh();
            dgPrint855.ItemsSource = EDI_855List;
            dg850Print.Items.Refresh();
        }

        private void btnPrintAnsi855_Click(object sender, RoutedEventArgs e)
        {
            if (dgPrint855.SelectedItems.Count > 0)
            {
                E_EDIOrders item = (E_EDIOrders)dgPrint855.SelectedItems[0];

                //Getting all segments for 855
                EDIElements(855);
                CreateANSI855(item.POnumber);

                //export to txt
                string ANSI = txtPrintEDI855.Text;
                string TextLine = "";
                // Set a variable to the My Documents path.
                string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                foreach (char C in ANSI)
                {
                    TextLine += C;
                    if (C == Convert.ToChar("~"))
                    {
                        //  TextLine += Environment.NewLine;
                    }
                }
                // Write the text to a new file named "WriteFile.txt".
                string txtname = "855 Notification - PO Number " + item.POnumber + " " + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                File.WriteAllText(System.IO.Path.Combine(mydocpath, txtname), TextLine);
                TextLine = string.Empty;

                 objBus.CreateANSI(855, item.POnumber , txtPrintEDI855.Text);
                 MessageBox.Show("CODIGO ANSI GUADADO CORRECTAMENTE!!", "SAVE ANSI", MessageBoxButton.OK, MessageBoxImage.Information);
                 EDI_855List.Clear();
        
   
            }
            else
            {
                MessageBox.Show("Por favor seleciona una orden", "NO ORDER SELECTED", MessageBoxButton.OK, MessageBoxImage.Error);
            }

          

        }

        private void CreateANSI855(string PO)
        {
            int CountSegments = 0;
            string TempDate = "";
            string LineAnsi = "";
            int Temp = EDI_Items[0].edsID;
            DataTable tdb855DataDetail = objBusEdi.get855DataDetail(PO);
            List<E_EDISeg> EDISegLines855 = new List<E_EDISeg>();
            Boolean Flag_Lines = false;
            foreach (E_EDISeg it in EDI_Items)
            {
                if (it.edtsID >= 207 && it.edtsID < 223)
                {
                    EDISegLines855.Add(it);
                }
                else if(it.edtsID == 223)
                {
                    EDISegLines855.Add(it);
                    //End Line
                  //  if (it.edsID != Temp)
                   // {
                        LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                        LineAnsi += "~";
                        txtPrintEDI855.Text += LineAnsi + Environment.NewLine;
                        LineAnsi = string.Empty;
                        CountSegments++;
                   // }
                 CountSegments +=  CreateANSI855Lines(EDISegLines855, tdb855DataDetail);
                 Flag_Lines = true;
               //  Temp = it.edsID;
                }
                else
                {
                    #region All Segments

                    if(Flag_Lines == true)
                    {
                        Temp = it.edsID;
                        Flag_Lines = false;
                    }

                    //New Line
                    if (it.edsID != Temp)
                    {
                        LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                        LineAnsi += "~";
                        txtPrintEDI855.Text += LineAnsi + Environment.NewLine;
                        LineAnsi = string.Empty;
                        CountSegments++;
                    }
                    //if it's defaul value from Layout
                    if (it.StaticFlag == 1)
                    {
                        LineAnsi += it.Value + "*";
                    }
                    else if (it.edtsID == 172) //ISA DATE
                    {
                        TempDate = DateTime.Now.ToString("yyMMdd");
                        LineAnsi += TempDate + "*";
                    }
                    else if (it.edtsID == 184) //GS DATE
                    {
                        TempDate = DateTime.Now.ToString("yyyyMMdd");
                        LineAnsi += TempDate + "*";
                    }
                    else if (it.edtsID == 173 || it.edtsID == 185) //ISA TIME
                    {
                        TempDate = DateTime.Now.ToString("HHmm");
                        LineAnsi += TempDate + "*";
                    }
                    else if (it.edtsID == 227) //SE Qty Segments
                    {
                        LineAnsi += (CountSegments - 1).ToString() + "*";
                    }
                    else if (it.edtsID == 230 || it.edtsID == 233) //GE Qty Invoices
                    {
                        LineAnsi += "1" + "*";
                    }
                    else if (it.Query.Contains("Grid"))
                    {
                        int Element = GetElementNumber(it.Query);
                        if (Element == 2)
                        {
                            LineAnsi += Convert.ToDateTime(tdb855DataDetail.Rows[0][Element].ToString()).ToString("yyyyMMdd") + "*";
                        }
                        else
                        {
                            LineAnsi += tdb855DataDetail.Rows[0][Element].ToString() + "*";
                        }

                    }
                    else if (it.Query.Contains("Items"))
                    {
                        LineAnsi += tdb855DataDetail.Rows.Count.ToString() + "*";
                    }
                    else if (it.Query != "")
                    {
                        if (it.edtsID == 176 || it.edtsID == 231 || it.edtsID == 234)
                        {
                            LineAnsi += CreateISAnumber(objBus.EDIQuery(it.Query, HomeDepotPO, 0)) + "*";
                        }
                        else if (it.edtsID == 186) //GS Control Software Number
                        {
                            LineAnsi += "00" + objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*";
                        }
                        else if (it.edtsID == 191 || it.edtsID == 228) //ST  and SE Control Software Number
                        {
                            LineAnsi += "0000" + objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*";
                        }
                        else
                        {
                            LineAnsi += objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*";
                        }
                    }
                    #endregion
                }
                Temp = it.edsID;
                
            }
            //Last Line
            LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
            LineAnsi += "~";
            txtPrintEDI855.Text += LineAnsi;
            LineAnsi = string.Empty;

        }

        private int CreateANSI855Lines(List<E_EDISeg> LIst, DataTable tdb)
        {
            int CountSegments = 0;
            string LineAnsi = "";
            int Temp = LIst[0].edsID;
            foreach(DataRow dr in tdb.Rows)
            {
                foreach (E_EDISeg it in LIst)
                {
                    //New Line
                    if (it.edsID != Temp)
                    {
                        LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                        LineAnsi += "~";
                        txtPrintEDI855.Text += LineAnsi + Environment.NewLine;
                        LineAnsi = string.Empty;
                        CountSegments++;
                    }
                    //if it's defaul value from Layout
                    if (it.StaticFlag == 1)
                    {
                        LineAnsi += it.Value + "*";
                        if(it.edtsID == 210) //Two Blanks
                        {
                            LineAnsi += "*";
                            LineAnsi += "*";
                        }
                    }
                    else if(it.edtsID == 221)
                    {
                        LineAnsi += DateTime.Now.AddDays(5).ToString("yyyyMMdd") + "*";
                        LineAnsi += "*"; //Blank
                    }
                    else if (it.Query.Contains("Grid"))
                    {
                        int Element = GetElementNumber(it.Query);
                        LineAnsi += dr[Element].ToString() + "*";
                    }
                    Temp = it.edsID;

                }
            }
            //LastLine
            LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
            LineAnsi += "~";
            txtPrintEDI855.Text += LineAnsi + Environment.NewLine;
            LineAnsi = string.Empty;
            CountSegments++;

            return CountSegments;

        }

        private int GetElementNumber(string Query)
        {
            int Position = Query.IndexOf(".") + 1;
            string Elemment = Query.Substring(Position, Query.Length - Position);

            return Convert.ToInt16(Elemment);

        }

        private string CreateISAnumber(string ControlNumber)
        {
            int Number = Convert.ToInt32(ControlNumber);

            if(Number < 10)
            {
                return "00000000" + Number.ToString();
            }
            else if(Number < 100)
            {
                return "0000000" + Number.ToString();
            }
            else if(Number < 1000)
            {
                return "000000" + Number.ToString();
            
            }
            else if(Number < 10000)
            {
                return "00000" + Number.ToString();
            }
            else if(Number < 100000)
            {
                return "0000" + Number.ToString();
            }
            else if(Number < 1000000)
            {
                return "000" + Number.ToString();
            }
            else if(Number < 10000000)
            {
                return "00" + Number.ToString();
            }
            else if(Number < 100000000)
            {
                return "0" + Number.ToString();
            }
            else
            {
                return Number.ToString();
            }

        }

        private void btn856toCreate_Click(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBusEdi.get856toCreate();
            foreach (DataRow dr in tdb.Rows)
            {
                E_EDIOrders it = new E_EDIOrders
                {
                    BWSOrder = dr[0].ToString(),
                    POnumber = dr[1].ToString(),
                    PODate = Convert.ToDateTime(dr[2].ToString()).ToShortDateString(),
                    Customer = dr[3].ToString(),
                    Address = dr[4].ToString(),
                    City = dr[5].ToString(),
                    State = dr[6].ToString(),
                    PostalCode = dr[7].ToString(),
                    AfiliateID = dr[8].ToString(),
                };
                EDI_856List.Add(it);
            }

            dgPrint856.ItemsSource = EDI_856List;

        }

        private void btnPrintAnsi856_Click(object sender, RoutedEventArgs e)
        {
            if(dgPrint856.SelectedItems.Count > 0)
            {
                E_EDIOrders item = (E_EDIOrders)dgPrint856.SelectedItems[0];
                Tracking frm = new Tracking();
                 frm.WindowStartupLocation = WindowStartupLocation.CenterScreen;
               // frm.txtQuantity.Text = row.Cantida.ToString();
                frm.ShowDialog();
                if (frm.DialogResult.HasValue && frm.DialogResult.Value)
                {

                    //Getting all segments for 856
                    EDIElements(856);
                    if (CreateANSI856(item.POnumber, frm.txtTracking.Text, frm.dpShip.SelectedDate.Value.ToString("yyyyMMdd")) == 0) 
                    {

                        //export to txt
                        string ANSI = txtPrintEDI856.Text;
                        string TextLine = "";
                        // Set a variable to the My Documents path.
                        string mydocpath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

                        foreach (char C in ANSI)
                        {
                            TextLine += C;
                            if (C == Convert.ToChar("~"))
                            {
                                //  TextLine += Environment.NewLine;
                            }
                        }
                        // Write the text to a new file named "WriteFile.txt".
                        string txtname = "856 Shipping - PO Number " + item.POnumber + " " + DateTime.Now.ToString("yyyyMMdd") + ".txt";
                        File.WriteAllText(System.IO.Path.Combine(mydocpath, txtname), TextLine);
                        TextLine = string.Empty;

                        objBus.CreateANSI(856, item.POnumber, txtPrintEDI856.Text);
                        objBusEdi.SaveShipment(item.POnumber, frm.dpShip.SelectedDate.Value.ToShortDateString(), frm.txtTracking.Text);
                        MessageBox.Show("CODIGO ANSI GUADADO CORRECTAMENTE!!", "SAVE ANSI", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                    else
                    {
                         MessageBox.Show("Cesar a esta orden le falta informacion de lineas, por favor Habla con Ruben", "Fala Informacion", MessageBoxButton.OK, MessageBoxImage.Stop);
                    }
                    EDI_856List.Clear();
                }

            }
            else
            {
                MessageBox.Show("Por favor seleciona una orden", "NO ORDER SELECTED", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private int CreateANSI856(string PO, string Tracking, string Date)
        {
            int CountSegments = 0;
            string TempDate = "";
            string LineAnsi = "";
            int Temp = EDI_Items[0].edsID;
            E_EDIOrders item = (E_EDIOrders)dgPrint856.SelectedItems[0];
            DataTable tdb856Lines = objBusEdi.getLineswithDetail(PO);
            List<E_EDISeg> EDISegLines856 = new List<E_EDISeg>();
            Boolean Flag_Lines = false;

            if (tdb856Lines.Rows.Count > 0)
            {

                foreach (E_EDISeg it in EDI_Items)
                {
                    if (it.edtsID >= 558 && it.edtsID < 569) //Ruben: 300 @ 311 *** BWS 558 @ 569
                    {
                        EDISegLines856.Add(it);
                    }
                    else if ((it.edtsID == 569)) //Ruben: 311 *** BWS: 569
                    {
                        EDISegLines856.Add(it);
                        LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                        LineAnsi += "~";
                        txtPrintEDI856.Text += LineAnsi + Environment.NewLine;
                        LineAnsi = string.Empty;
                        CountSegments++;

                        CountSegments += CreateANSI856Lines(EDISegLines856, tdb856Lines);
                        Flag_Lines = true;
                        //  Temp = it.edsID;
                    }
                    else
                    {
                        if (Flag_Lines == true)
                        {
                            Temp = it.edsID;
                            Flag_Lines = false;
                        }

                        //New Line
                        if (it.edsID != Temp)
                        {
                            LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                            LineAnsi += "~";
                            txtPrintEDI856.Text += LineAnsi + Environment.NewLine;
                            LineAnsi = string.Empty;
                            CountSegments++;
                        }
                        //if it's defaul value from Layout
                        if (it.StaticFlag == 1)
                        {
                            LineAnsi += it.Value + "*";
                        }
                        else if (it.edtsID == 502) //ISA DATE Ruben: 244 ** BWS: 502
                        {
                            TempDate = DateTime.Now.ToString("yyMMdd");
                            LineAnsi += TempDate + "*";
                        }
                        else if (it.edtsID == 514) //GS DATE Ruben: 256 ** BWS: 514
                        {
                            TempDate = DateTime.Now.ToString("yyyyMMdd");
                            LineAnsi += TempDate + "*";
                        }
                        else if (it.edtsID == 503 || it.edtsID == 515 || it.edtsID == 526) //ISA AND GS TIME Ruben: 245, 257, 268 ** BWS: 503, 515, 526
                        {
                            TempDate = DateTime.Now.ToString("HHmm");
                            LineAnsi += TempDate + "*";
                        }
                        else if (it.edtsID == 525) //Order Ship Date Ruben: 267 ** BWS: 525
                        {
                            LineAnsi += Date + "*";
                        }
                        else if (it.edtsID == 538) //Order Trackin, Ruben: 280 ** BWS: 538
                        {
                            LineAnsi += Tracking + "*";
                        }
                        else if (it.edtsID == 541) //Order Customer, Ruben: 283 ** BWS: 541
                        {
                            LineAnsi += item.Customer + "*";
                        }
                        else if (it.edtsID == 543) //Order Adress, Ruben: 285 ** BWS: 543
                        {
                            LineAnsi += item.Address + "*";
                        }
                        else if (it.edtsID == 545) //Order Adress, Ruben: 287 ** BWS: 545
                        {
                            LineAnsi += item.City + "*";
                        }
                        else if (it.edtsID == 546) //Order Adress, Ruben: 288 ** BWS: 546
                        {
                            LineAnsi += item.State + "*";
                        }
                        else if (it.edtsID == 547) //Order Adress, Ruben: 289 ** BWS: 547
                        {
                            LineAnsi += item.PostalCode + "*";
                        }
                        else if (it.edtsID == 553) //PO NUMBER, Ruben: 295 ** BWS: 553
                        {
                            LineAnsi += item.POnumber + "*";
                        }
                        else if (it.edtsID == 557) //Afiliate ID, Ruben: 299 ** BWS: 557
                        {
                            if (item.AfiliateID == "03") //Binds.com
                            {
                                LineAnsi += "BC2014000" + "*";
                            }
                            else
                            {
                                LineAnsi += "AM2014001" + "*";
                            }
                        }
                        else if (it.Query.Contains("QtySegments")) //SE Qty Segments
                        {
                            LineAnsi += (CountSegments - 1).ToString() + "*";
                        }
                        else if (it.Query.Contains("Items")) //CCT Lines
                        {
                            LineAnsi += tdb856Lines.Rows.Count.ToString() + "*";
                        }
                        else if (it.Query.Contains("Grid"))
                        {

                        }
                        else if (it.Query != "")
                        {
                            if (it.edtsID == 506 || it.edtsID == 577 || it.edtsID == 580) //ISA CONTROL SOFTWARE, Ruben: 248, 319, 322 ** BWS: 506, 577, 580
                            {
                                LineAnsi += CreateISAnumber(objBus.EDIQuery(it.Query, HomeDepotPO, 0)) + "*"; //   Quitar el 1 cuando empiece Cesar
                            }
                            else if (it.edtsID == 516) //GS Control Software Number, Ruben: 258 ** BWS: 516
                            {
                                LineAnsi += "00" + objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*"; //   Quitar el 1 cuando empiece Cesar
                            }
                            else if (it.edtsID == 521 || it.edtsID == 574) //ST  and SE Control Software Number, Ruben: 263, 316 ** BWS: 521, 574
                            {
                                LineAnsi += "0000" + objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*"; //   Quitar el 1 cuando empiece Cesar
                            }
                            else if (it.edtsID == 524) //Shipments number, Ruben: 266 ** BWS: 524
                            {
                                LineAnsi += objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*";
                            }
                            else
                            {
                                LineAnsi += objBus.EDIQuery(it.Query, HomeDepotPO, 0) + "*";
                            }
                        }

                    }
                    Temp = it.edsID;
                }

                LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                LineAnsi += "~";
                txtPrintEDI856.Text += LineAnsi;
                LineAnsi = string.Empty;
                return 0;
            }
            else
            {
               
                return 1;
            }

        }

        private int CreateANSI856Lines(List<E_EDISeg> LIst, DataTable tdb)
        {
            int CountSegments = 0;
             string LineAnsi = "";
            int Temp = LIst[0].edsID;
            int CountLInes = 2;
            foreach (DataRow dr in tdb.Rows)
            {
                CountLInes++;
                foreach (E_EDISeg it in LIst)
                {
                    //New Line
                    if (it.edsID != Temp)
                    {
                        LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
                        LineAnsi += "~";
                        txtPrintEDI856.Text += LineAnsi + Environment.NewLine;
                        LineAnsi = string.Empty;
                        CountSegments++;
                    }
                    //if it's defaul value from Layout
                    if (it.StaticFlag == 1)
                    {
                        LineAnsi += it.Value + "*";
                        
                    }
                    else if (it.edtsID == 559) //Ruben: 301 ** BWS: 559
                    {
                        LineAnsi += CountLInes.ToString() + "*";
                    }
                    else if (it.Query.Contains("Grid"))
                    {
                        int Element = GetElementNumber(it.Query);
                        LineAnsi += dr[Element].ToString() + "*";
                    }
                    Temp = it.edsID;

                }
            }

            LineAnsi = LineAnsi.Substring(0, LineAnsi.Length - 1);
            LineAnsi += "~";
            txtPrintEDI856.Text += LineAnsi + Environment.NewLine;
            LineAnsi = string.Empty;
            CountSegments++;


            return CountSegments;
        }

        private void btn810toCreate_Click(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBusEdi.get810toCreate();

            foreach(DataRow dr in tdb.Rows)
            {
                E_810toCreate it = new E_810toCreate
                {
                    BWSOrder = dr[0].ToString(),
                    PONumber = dr[1].ToString(),
                    PODate = Convert.ToDateTime(dr[2].ToString()).ToShortDateString(),
                    Customer = dr[3].ToString(),
                    Address = dr[4].ToString(),
                    City = dr[5].ToString(),
                    State = dr[6].ToString(),
                    PostalCode = dr[7].ToString(),
                    AffiliateID = dr[8].ToString(),
                    Tracking = dr[9].ToString(),
                    ShipOn = Convert.ToDateTime(dr[10].ToString()).ToShortDateString()

                };
                EDI_810List.Add(it);
            }

            dgPO.ItemsSource = EDI_810List;
        }
    }
}
