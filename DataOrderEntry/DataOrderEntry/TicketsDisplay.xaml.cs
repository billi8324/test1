﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.SqlClient;
using System.Data;
using Business;
using Entity;
using excel = Microsoft.Office.Interop.Excel;

namespace DataOrderEntry
{
    /// <summary>
    /// Interaction logic for TicketsDisplay.xaml
    /// </summary>
    public partial class TicketsDisplay : UserControl
    {
        B_Order objBus = new B_Order();
        List<TicketsCreated> Items = new List<TicketsCreated>();
        int SerialID = 0;

        public TicketsDisplay()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            DataTable tdb = objBus.TicketsCreated();

            foreach(DataRow rw in tdb.Rows)
            {
                TicketsCreated it = new TicketsCreated
                {
                    Serial = Convert.ToInt32(rw[0].ToString()),
                    OrderID = rw[1].ToString(),
                    SaleDate = Convert.ToDateTime(rw[2].ToString()).ToShortDateString(),
                    DueDate = Convert.ToDateTime(rw[3].ToString()).ToShortDateString(),
                    Sidemark = rw[4].ToString(),
                    Product = rw[5].ToString(),
                    OperatingSystem = rw[6].ToString()
                };
                Items.Add(it);
            }

            dgSumary.ItemsSource = Items;
        }

        private void dgSumary_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(dgSumary.SelectedItems.Count > 0)
            {
                TicketsCreated ticket = (TicketsCreated)dgSumary.SelectedItems[0];
                DataTable tdb = objBus.SerialDetails(ticket.Serial);
                SerialID = ticket.Serial;
                List<E_TicketDisplay> ListTickets = new List<E_TicketDisplay>();

                foreach(DataRow dr in tdb.Rows)
                {
                    E_TicketDisplay it = new E_TicketDisplay
                    {
                        Serial = Convert.ToInt32(dr[0].ToString()),
                        OptionID = Convert.ToInt32(dr[1].ToString()),
                        OptionName = dr[2].ToString(),
                        Value = dr[3].ToString()
                    };
                    ListTickets.Add(it);
                }


                dgDetails.ItemsSource = ListTickets;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DataTable tdbHeader = objBus.TicketPrintHeader(SerialID);

            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            ExcelApp.Application.Workbooks.Add(Type.Missing);

            int row, col, j;
            excel.Range ChartRange;

            excel.Worksheet sheet = (excel.Worksheet)ExcelApp.Sheets[1];

            row = 1;
            col = 1;

            #region Head for Report
            ChartRange = ExcelApp.get_Range("A" + row, "F" + (row + 1));
            ChartRange.Merge();
            ChartRange.Font.Bold = true;
            ChartRange.HorizontalAlignment = excel.XlHAlign.xlHAlignCenter;
            ChartRange.VerticalAlignment = excel.XlHAlign.xlHAlignCenter;
            ChartRange.Value = "ROMAN SHADE - " + tdbHeader.Rows[0][6].ToString();
            ChartRange.Interior.ColorIndex = 19;
            ChartRange.Borders.Weight = 1;
            row++;
            row++;

            ExcelApp.Cells[row, 1].Value = "CUSTOMER NAME";
            ExcelApp.Cells[row, 1].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 1].Font.Bold = true;
            ExcelApp.Cells[row, 1].ColumnWidth = 22.00;
            ExcelApp.Cells[row, 1].Borders.Weight = 1;
            ChartRange = ExcelApp.get_Range("B" + row, "B" + row);
            ChartRange.Merge();
            ChartRange.Borders.Weight = 1;
            ChartRange.Value = tdbHeader.Rows[0][4].ToString();

            ExcelApp.Cells[row, 3].Value = "ORDER ID";
            ExcelApp.Cells[row, 3].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 3].Font.Bold = true;
            ExcelApp.Cells[row, 3].Borders.Weight = 1;
            ExcelApp.Cells[row, 4].Value = tdbHeader.Rows[0][3].ToString();
            ExcelApp.Cells[row, 4].Font.Bold = true;
            ExcelApp.Cells[row, 4].Borders.Weight = 1;

            ExcelApp.Cells[row, 5].Value = "SALE DATE";
            ExcelApp.Cells[row, 5].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 5].Font.Bold = true;
            ExcelApp.Cells[row, 5].Borders.Weight = 1;
            ExcelApp.Cells[row, 6].Value = Convert.ToDateTime(tdbHeader.Rows[0][1].ToString()).ToShortDateString();
            ExcelApp.Cells[row, 6].Font.Bold = true;
            ExcelApp.Cells[row, 6].Borders.Weight = 1;

            row++;

            ExcelApp.Cells[row, 1].Value = "SIDEMARK";
            ExcelApp.Cells[row, 1].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 1].Font.Bold = true;
            ExcelApp.Cells[row, 1].ColumnWidth = 22.00;
            ExcelApp.Cells[row, 1].Borders.Weight = 1;
            ExcelApp.Cells[row, 2].Borders.Weight = 1;
            ExcelApp.Cells[row, 2].ColumnWidth = 41.00;
            ExcelApp.Cells[row, 2].Value = tdbHeader.Rows[0][5].ToString();

            ExcelApp.Cells[row, 3].Value = "SERIAL ID";
            ExcelApp.Cells[row, 3].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 3].Font.Bold = true;
            ExcelApp.Cells[row, 3].ColumnWidth = 16.00;
            ExcelApp.Cells[row, 3].Borders.Weight = 1;
            ChartRange = ExcelApp.get_Range("D" + row, "D" + row);
            ChartRange.Merge();
            ChartRange.Borders.Weight = 1;
            ChartRange.Value = tdbHeader.Rows[0][0].ToString();

            ExcelApp.Cells[row, 5].Value = "DUE DATE";
            ExcelApp.Cells[row, 5].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 5].Font.Bold = true;
            ExcelApp.Cells[row, 5].ColumnWidth = 16.00;
            ExcelApp.Cells[row, 5].Borders.Weight = 1;
            ExcelApp.Cells[row, 5].HorizontalAlignment = excel.XlHAlign.xlHAlignCenter;
            ChartRange = ExcelApp.get_Range("F" + row, "F" + row);
            ChartRange.Merge();
            ChartRange.Borders.Weight = 1;
            ChartRange.Value = Convert.ToDateTime(tdbHeader.Rows[0][2].ToString()).ToShortDateString();

            row++;

            ExcelApp.Cells[row, 1].Value = "OPERATION";
            ExcelApp.Cells[row, 1].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 1].Font.Bold = true;
            ExcelApp.Cells[row, 1].Borders.Weight = 1;
            ChartRange = ExcelApp.get_Range("B" + row, "B" + row);
            ChartRange.Merge();
            ChartRange.Borders.Weight = 1;
            ChartRange.Value = tdbHeader.Rows[0][7].ToString();

            ExcelApp.Cells[row, 3].Value = "ORDER SIZE";
            ExcelApp.Cells[row, 3].Interior.ColorIndex = 19;
            ExcelApp.Cells[row, 3].Font.Bold = true;
            ExcelApp.Cells[row, 3].ColumnWidth = 16.00;
            ExcelApp.Cells[row, 3].Borders.Weight = 1;
            ChartRange = ExcelApp.get_Range("D" + row, "D" + row);
            ChartRange.Merge();
            ChartRange.Borders.Weight = 1;
            ChartRange.Value = tdbHeader.Rows[0][8].ToString() + " * " + tdbHeader.Rows[0][9].ToString();

           // row++;
           // ChartRange = ExcelApp.get_Range("A" + row, "F" + row);
           // ChartRange.Borders.Weight = 1;
           // ChartRange.Interior.ColorIndex = 35;

            row++;

            #endregion



            for (j = 0; j < dgDetails.Columns.Count; j++)
            {
                ExcelApp.Cells[row, col] = dgDetails.Columns[j].Header;
                ExcelApp.Cells[row, col].Borders.Weight = 2;
                ExcelApp.Cells[row, col].Interior.ColorIndex = 15;
                //ExcelApp.Cells[row, col].Font.ColorIndex = 2;
                ExcelApp.Cells[row, col].VerticalAlignment = excel.XlVAlign.xlVAlignCenter;
                ExcelApp.Cells[row, col].HorizontalAlignment = excel.XlVAlign.xlVAlignCenter;

                col++;
            }

            row++;

            for (int i = 0; i < dgDetails.Items.Count - 1; i++)
            {
                E_TicketDisplay gridrow = (E_TicketDisplay)dgDetails.Items[i];
               
                ExcelApp.Cells[row, 1] = gridrow.Serial;
                ExcelApp.Cells[row, 2] = gridrow.OptionID;
                ExcelApp.Cells[row, 3] = gridrow.OptionName;
                ExcelApp.Cells[row, 4] = gridrow.Value;
                ExcelApp.Cells[row, 4].HorizontalAlignment = excel.XlHAlign.xlHAlignRight;


                // ChartRange = ExcelApp.get_Range("A" + row, "c" + row);
                // ChartRange.Borders.Weight = 2;

                row++;
            }


            ExcelApp.Columns.AutoFit(); //Ajusta ancho de todas las columnas
            ExcelApp.Rows.AutoFit();
            //ExcelApp.Columns[1].Width = 10;
            ExcelApp.ActiveWindow.DisplayGridlines = false;
            ExcelApp.Visible = true;
        }
    }
}
