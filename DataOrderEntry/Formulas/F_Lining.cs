﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas
{
    public class F_Lining
    {
        public decimal LiningWidth { get; set; }
        public decimal LateralLinig { get; set; }
        public decimal LiningLength { get; set; }

        public F_Lining(decimal Width, string LiningType, decimal FabricWidth, decimal FinalLateral, decimal FabricLength)
        {
            if (LiningType.Contains("PRIVACY"))
            {
                if (Width <= 48)
                {
                    LiningWidth = (Width - Convert.ToDecimal("0.5")) + ((Convert.ToDecimal("1.25")) * 2);

                }
                else
                {
                    LiningWidth = FabricWidth;
                    LateralLinig = FinalLateral + Convert.ToDecimal("0.5") - Convert.ToDecimal("0.25") + Convert.ToDecimal("1.25");
                }
            }
            else
            {
                if (Width <= 48)
                {
                    LiningWidth = Width - Convert.ToDecimal("0.25");

                }
                else
                {
                    LiningWidth = FabricWidth;
                    LateralLinig = FinalLateral + Convert.ToDecimal("0.5") - Convert.ToDecimal("0.25");
                }
            }

            CalculateLiningLength(FabricLength);

        }

       private void CalculateLiningLength(decimal FabricLength)
        {
            LiningLength = FabricLength;
        }
    }
}
