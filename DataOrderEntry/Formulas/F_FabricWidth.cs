﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas
{
    public class F_FabricWidth
    {
        public decimal FabricWidth(decimal Width)
        {
            if(Width <= 48)
            {
                return Width + 6;
            }
            else
            {
                if((Width-54)/2 <= 8)
                {
                    return (Width - 18) + 1;
                }
                else
                {
                    return 52+1;
                }
            }
        }
    }
}
