﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas
{
    public class F_HRsize
    {
        private string _Mount;
        private decimal HRSize;
        private decimal _Width;

        public decimal getHeadrailSize(string Mount, decimal Width)
        {
            _Mount = Mount;
            _Width = Width;
            if(Mount == "Inside")
            {
                Inside();
            }
            else
            {
                Outside();
            }
            return HRSize;
        }

        private void Inside()
        {
               HRSize = _Width - Convert.ToDecimal("0.25");
        }

        private void Outside()
        {
             HRSize = _Width + Convert.ToDecimal("0.25");
        }


    }
}
