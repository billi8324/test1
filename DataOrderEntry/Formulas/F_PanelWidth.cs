﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas
{
    public class F_PanelWidth
    {
        public decimal PanelWidth { get; set; }
        public decimal FinalPanelWidth { get; set; }

        public F_PanelWidth(decimal Width, decimal FabricWidth)
        {
            if ((Width - 54) / 2 <= 8)
            {
                PanelWidth = ((Width - FabricWidth) / 2) + 2 + 3 + Convert.ToDecimal("0.5");
                FinalPanelWidth = (Width - FabricWidth) / 2;
            }
            else
            {
                PanelWidth = ((Width - 52) / 2) + 3 + Convert.ToDecimal("0.5");
                FinalPanelWidth = (Width - 52) / 2;
            }
        }
    }
}
