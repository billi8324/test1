﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Business;

namespace Formulas
{
    public class F_FinishWidth
    {
        B_Order objbus = new B_Order();

        public decimal getFinishWidth(string HRType, string Mount, decimal Width, string Gad)
        {
            decimal _FinishWidth = 0;
            if(Mount == "Inside")
            {
                if(HRType == "2 IN 1")
                {
                    _FinishWidth = Width - Convert.ToDecimal("0.25");
                }
                else
                {
                    _FinishWidth = Width - Convert.ToDecimal("0.25");
                }
            }
            else if(Mount == "Outside")
            {
                if (HRType == "2 IN 1")
                {
                    _FinishWidth = Width;
                }
                else
                {
                    _FinishWidth = Width;
                }
            }


            return _FinishWidth;
        }
    }
}
