﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas
{
    public class F_HeadrailPart
    {
        public string HeadrailPart(string OperationSystem)
        {
            string HRpart ="";
            switch(OperationSystem)
            {
                case "Cordlock":
                    HRpart = "HR 1 1/4 in";
                    break;
                case "Cordless":
                    HRpart = "HR 2 in";
                    break;
                case "CCO":
                    HRpart = "HR 2 in";
                    break;
                case "Motorized":
                    HRpart = "HR 3 in";
                    break;
                case "TDBU":
                    HRpart = "HR 1 1/4 in";
                    break; 

            }

            return HRpart;
        }
    }
}
