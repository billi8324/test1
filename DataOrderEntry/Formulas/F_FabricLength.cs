﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Formulas
{
    public class F_FabricLength
    {
        public decimal FabricLength { get; set; }

        public F_FabricLength(decimal Width)
        {
            FabricLength = Width + Convert.ToDecimal("8.5") + Convert.ToDecimal("1.25") + Convert.ToDecimal("0.75") + Convert.ToDecimal("1.25")
                + Convert.ToDecimal("0.75") + Convert.ToDecimal("4.25");
        }
    }
}
