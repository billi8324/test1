﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace Business
{
    public class B_EDI
    {
        public D_EDI objEDI = new D_EDI();


        public DataTable getSerialDetails(string PO, int Line)
        {
            return objEDI.getSerialDetails(PO, Line);
        }

        public int getOptionID(string OptionName)
        {
            return objEDI.getOptionID(OptionName);
        }

        public void SaveSerialDetails(int OptionID, string Value, int Line)
        {
            objEDI.SaveSerialDetails(OptionID, Value, Line);
        }

        public DataTable OrderReport(string PO)
        {
            return objEDI.OrderReport(PO);
        }

        public DataTable getLines(string PO)
        {
            return objEDI.getLines(PO);
        }

        public DataTable OrderstoPrint()
        {

            return objEDI.OrderstoPrint();
        }

        public DataTable get855toCreate()
        {
            return objEDI.get855toCreate();
        }

        public DataTable get855DataDetail(string PO)
        {
            return objEDI.get855DataDetail(PO);
        }

        public DataTable get856toCreate()
        {
            return objEDI.get856toCreate();
        }

        public DataTable getLineswithDetail(string PO)
        {
            return objEDI.getLineswithDetail(PO);
        }

        public void SaveShipment(string PO, string Date, string Tracking)
        {
            objEDI.SaveShipment(PO, Date, Tracking);
        }

        public int OrderExists(string PO)
        {
            return objEDI.OrderExists(PO);
        }

        public string CreateL3invoice(string PO)
        {
          return objEDI.CreateL3invoice(PO);
        }

        public DataTable get810toCreate()
        {
            return objEDI.get810toCreate();
        }
    }
}
