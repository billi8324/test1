﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using System.Data;


namespace Business
{
    public class B_Order
    {
        public D_Order objData = new D_Order();
        public D_EDI objEDI = new D_EDI();

        public int Pockets(decimal Length)
        {
            return objData.Pockets(Length);
        }

        public string NewOrderNO(int CustomerID, string PO, string POdate)
        {
            return objData.NewOrderNO(CustomerID, PO, POdate);
        }

        public void BuildSeria(string orderid, int Serial, int OptionID, string Value)
        {
            objData.BuildSeria(orderid, Serial, OptionID, Value);
        }

        public int NewSerialID(string OrderID, int ProductID, int Line, int ShipType, string SaleDate, string DueDate, decimal Cost)
        {
            return objData.NewSerialID(OrderID, ProductID, Line, ShipType, SaleDate, DueDate, Cost);
        }

        public int NewCustomerID()
        {
            return objData.NewCustomerID();
        }

        public void NewCustomerData(int ID, string name, string company, string Address, int zip, string City, string State, string Country, string Phone,
                                    string Email, string Sidemark)
        {
            objData.NewCustomerData(ID, name, company, Address, zip, City, State, Country, Phone, Email, Sidemark);
        }

        public DataTable CustomerList()
        {
            return objData.CustomerList();
        }

        public DataTable ProductsList()
        {
            return objData.ProductsList();
        }

        public DataTable ProductType(int ProductID)
        {
            return objData.ProductType(ProductID);
        }

        public DataTable Carrier()
        {
            return objData.Carrier();
        }

        public DataTable OrderPrority()
        {
            return objData.OrderPrority();
        }

        public DataTable MaterialList(int Category)
        {
            return objData.MaterialList(Category);
        }

        public DataTable ProductFractions()
        {
            return objData.ProductFractions();
        }

        public DataTable Oprationsystem(int ProductID, int OptionID)
        {
            return objData.Oprationsystem(ProductID, OptionID);
        }

        public DataTable Headrail(int ProductID, int OptionID, int OperationType)
        {
            return objData.Headrail(ProductID, OptionID, OperationType);
        }

        public DataTable BuildSerialOptions(int ProductID, int Serial)
        {
            return objData.BuildSerialOptions(ProductID, Serial);
        }

        public string BuildSerial_GetoptionValue(int OptionID)
        {
            return objData.BuildSerial_GetoptionValue(OptionID);
        }

        public decimal GetFractionValue(string Display)
        {
            return objData.GetFractionValue(Display);
        }

        public DataTable TicketsCreated()
        {
            return objData.TicketsCreated();
        }

        public DataTable SerialDetails(int Serial)
        {
            return objData.SerialDetails(Serial);
        }

        public DataTable TicketPrintHeader(int Serial)
        {
            return objData.TicketPrintHeader(Serial);
        }

        public DataTable Ordersnot810()
        {
            return objData.Ordersnot810();
        }

        public void EDI_UpdateBWSData(string Invoice, string Tracking, string Date, string HomeDepotPO)
        {
            objData.EDI_UpdateBWSData(Invoice, Tracking, Date, HomeDepotPO);
        }

        public DataTable EDIElements(int DocumentID)
        {
            return objData.EDIElements(DocumentID);
        }

        public string EDIQuery(string Query, string PO, int Flag_Paramater)
        {
            return objData.EDIQuery(Query, PO, Flag_Paramater);
        }

        public void CreateANSI(int ediID, string PO, string Ansi)
        {
            objData.CreateANSI(ediID, PO, Ansi);
        }

        public DataTable EDIPrint(int DocumentID)
        {
            return objData.EDIPrint(DocumentID);
        }

        public string GettingANSI(int DocumentID, string PO)
        {
            return objData.GettingANSI(DocumentID, PO);
        }

        public DataTable EDI850Segments(string SegID)
        {
            return objData.EDI850Segments(SegID);
        }

        public void EDIcreateOrder(int edtsID, int edsID, int edID, string Value, int Element, string PO, int Line)
        {
            objData.EDIcreateOrder(edtsID, edsID, edID, Value, Element, PO, Line);
        }

        public DataTable getOrderHeader(string PO)
        {
            return objEDI.getOrderHeader(PO);
        }

        public string NewOrderNO(int CustomerID, string PO, string POdate, string POcustomer, string POaddress, string POCity, string POstate, string POpostal)
        {
            return objEDI.NewOrderNO(CustomerID, PO, POdate, POcustomer, POaddress, POCity, POstate, POpostal);
        }

        public DataTable getSerialHeader(string PO, int Line)
        {
            return objEDI.getSerialHeader(PO, Line);
        }

    }
}
